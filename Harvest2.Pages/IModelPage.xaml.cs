using Harvest2.ViewModels;
using System;
namespace Harvest2.Pages
{
	internal interface IModelPage
	{
		ViewModelBase Model
		{
			get;
			set;
		}
	}
}
