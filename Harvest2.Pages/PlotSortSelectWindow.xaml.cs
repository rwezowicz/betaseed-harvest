using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class PlotSortSelectWindow : Window, IComponentConnector
	{
		private PlotDirections _plotDirection;
		public PlotDirections PlotDirection
		{
			get
			{
				return this._plotDirection;
			}
			set
			{
				this._plotDirection = value;
				this.SetPlotDirectionRadio();
			}
		}
		public PlotSortSelectWindow()
		{
			this.InitializeComponent();
		}
		private void SetPlotDirectionRadio()
		{
			switch (this.PlotDirection)
			{
			case PlotDirections.Clock:
				this.ClockRadio.IsChecked = new bool?(true);
				return;
			case PlotDirections.AntiClock:
				this.AntiClockRadio.IsChecked = new bool?(true);
				return;
			default:
				return;
			}
		}
		private PlotDirections GetPlotDirection()
		{
			if (this.ClockRadio.IsChecked.HasValue && this.ClockRadio.IsChecked.Value)
			{
				return PlotDirections.Clock;
			}
			return PlotDirections.AntiClock;
		}
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}
		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			this._plotDirection = this.GetPlotDirection();
			base.DialogResult = new bool?(true);
			base.Close();
		}
		private void ImageClock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.ClockRadio.IsChecked = new bool?(true);
		}
		private void ImageAntiClock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			this.AntiClockRadio.IsChecked = new bool?(true);
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/plotsortselectwindow.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        this.ClockRadio = (RadioButton)target;
		//        return;
		//    case 2:
		//        this.AntiClockRadio = (RadioButton)target;
		//        return;
		//    case 3:
		//        this.ImageClock = (Image)target;
		//        this.ImageClock.MouseLeftButtonDown += new MouseButtonEventHandler(this.ImageClock_MouseLeftButtonDown);
		//        return;
		//    case 4:
		//        this.ImageAntiClock = (Image)target;
		//        this.ImageAntiClock.MouseLeftButtonDown += new MouseButtonEventHandler(this.ImageAntiClock_MouseLeftButtonDown);
		//        return;
		//    case 5:
		//        this.OkButton = (Button)target;
		//        this.OkButton.Click += new RoutedEventHandler(this.OkButton_Click);
		//        return;
		//    case 6:
		//        this.CancelButton = (Button)target;
		//        this.CancelButton.Click += new RoutedEventHandler(this.CancelButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
