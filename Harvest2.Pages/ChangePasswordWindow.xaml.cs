using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class ChangePasswordWindows : Window, IComponentConnector
	{
		private ChangePasswordViewModel _model;
		public ChangePasswordViewModel Model
		{
			get
			{
				return this._model;
			}
			set
			{
				this._model = value;
				base.DataContext = this._model;
			}
		}
		public ChangePasswordWindows(ChangePasswordViewModel model)
		{
			this.Model = model;
			this.InitializeComponent();
		}
		private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
		{
			this.Model.OldPassword = this.OldPasswordBox.Password.Trim();
			this.Model.NewPassword = this.NewPasswordBox.Password.Trim();
			this.Model.ConfirmPassword = this.ConfirmPasswordBox.Password.Trim();
			if (this.Model.ValidateModel())
			{
				this.Model.SetNewPassword();
				base.DialogResult = new bool?(true);
				base.Close();
			}
		}
		private void ChangePasswordWindow_KeyDown(object sender, KeyEventArgs e)
		{
			Key key = e.Key;
			if (key == Key.Return)
			{
				this.ChangePasswordButton_Click(sender, null);
				return;
			}
			if (key != Key.Escape)
			{
				return;
			}
			base.DialogResult = new bool?(false);
			base.Close();
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/changepasswordwindow.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        ((ChangePasswordWindows)target).KeyDown += new KeyEventHandler(this.ChangePasswordWindow_KeyDown);
		//        return;
		//    case 2:
		//        this.ErrorsLabel = (Label)target;
		//        return;
		//    case 3:
		//        this.OldPasswordBox = (PasswordBox)target;
		//        return;
		//    case 4:
		//        this.NewPasswordBox = (PasswordBox)target;
		//        return;
		//    case 5:
		//        this.ConfirmPasswordBox = (PasswordBox)target;
		//        return;
		//    case 6:
		//        this.ChangePasswordButton = (Button)target;
		//        this.ChangePasswordButton.Click += new RoutedEventHandler(this.ChangePasswordButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
