using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class SelectSheetWindow : Window, IComponentConnector
	{
		private IEnumerable<string> _sheets;
		public int SelectedSheet
		{
			get;
			set;
		}
		public SelectSheetWindow(IEnumerable<string> sheets)
		{
			this._sheets = sheets;
			this.InitializeComponent();
			this.SelectSheetCombo.ItemsSource = this._sheets;
			this.SelectSheetCombo.SelectedIndex = 0;
		}
		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			this.SelectedSheet = this.SelectSheetCombo.SelectedIndex;
			base.DialogResult = new bool?(true);
			base.Close();
		}
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = new bool?(false);
			base.Close();
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/selectsheetwindow.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        this.SelectSheetCombo = (ComboBox)target;
		//        return;
		//    case 2:
		//        this.OkButton = (Button)target;
		//        this.OkButton.Click += new RoutedEventHandler(this.OkButton_Click);
		//        return;
		//    case 3:
		//        this.CancelButton = (Button)target;
		//        this.CancelButton.Click += new RoutedEventHandler(this.CancelButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
