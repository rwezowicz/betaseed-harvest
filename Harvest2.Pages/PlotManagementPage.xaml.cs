using Harvest2.Helpers;
using Harvest2.Models;
using Harvest2.ViewModels;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class PlotManagementPage : Page, IComponentConnector, IStyleConnector
	{
		private PlotManagementViewModel _model;
		public PlotManagementViewModel Model
		{
			get
			{
				return this._model;
			}
			set
			{
				this._model = value;
				base.DataContext = this._model;
			}
		}
		public PlotManagementPage(PlotManagementViewModel model)
		{
			this.InitializeComponent();
			this.Model = model;
			this.Model.PlotDirection = PlotDirections.Clock;
			this.Model.LoadPlots();
		}
		private void LogoutButton_Click(object sender, RoutedEventArgs e)
		{
			this.Model.LogOut();
		}
		private void PlotItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			PlotDetailsViewModel plot = (sender as Border).DataContext as PlotDetailsViewModel;
			this.Model.PlotSelected(plot);
		}
		private void PlotManagement_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			this.Model.SetScaleTransform(base.ActualWidth, base.ActualHeight);
		}
		private void PlotImportButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (NavigationContext.UserData != null)
				{
					base.IsEnabled = false;
					base.Cursor = System.Windows.Input.Cursors.Hand;
					Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
					openFileDialog.Multiselect = false;
					openFileDialog.CheckFileExists = true;
					openFileDialog.Filter = "Excel files (*.xls, *.xlsx)|*.xls;*.xlsx";
					bool? flag = openFileDialog.ShowDialog();
					if (flag.HasValue && flag.Value)
					{
						this.Model.ImportPlots(openFileDialog.FileName);
					}
				}
				else
				{
					LoginViewModel model = new LoginViewModel();
					NavigationContext.LogOut();
					NavigationContext.NavigateToViewModel(model);
				}
			}
			finally
			{
				base.IsEnabled = true;
				base.Cursor = System.Windows.Input.Cursors.Arrow;
			}
		}
		private void ExportButton_Click(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
			saveFileDialog.AddExtension = true;
			saveFileDialog.CheckPathExists = true;
			saveFileDialog.CheckFileExists = false;
			saveFileDialog.CreatePrompt = true;
			saveFileDialog.DefaultExt = ".xls";
			saveFileDialog.Filter = "Excel file(*.xls)|*.xls";
			bool? flag = saveFileDialog.ShowDialog();
			if (flag.HasValue && flag.Value)
			{
				this.Model.Export(saveFileDialog.FileName);
			}
		}
		private void PlotDirection_Click(object sender, RoutedEventArgs e)
		{
			PlotSortSelectWindow plotSortSelectWindow = new PlotSortSelectWindow();
			System.Drawing.Point mousePosition = System.Windows.Forms.Control.MousePosition;
			plotSortSelectWindow.Left = (double)mousePosition.X - plotSortSelectWindow.Width - 20.0;
			plotSortSelectWindow.Top = (double)mousePosition.Y;
			plotSortSelectWindow.PlotDirection = this.Model.PlotDirection;
			bool? flag = plotSortSelectWindow.ShowDialog();
			if (flag.HasValue && flag.Value)
			{
				this.Model.PlotDirection = plotSortSelectWindow.PlotDirection;
			}
		}
		private void Border_TargetUpdated(object sender, DataTransferEventArgs e)
		{
			if (NavigationContext.Model is PlotDetailsViewModel && e.Source is Border)
			{
				Border border = (Border)e.Source;
				PlotDetailsViewModel plotDetailsViewModel = border.DataContext as PlotDetailsViewModel;
				if (plotDetailsViewModel != null && plotDetailsViewModel.PlotState == PlotStates.InEditing)
				{
					System.Windows.Point point = border.TranslatePoint(new System.Windows.Point(0.0, 0.0), this.ItemsScroll);
					if (point.X > this.ItemsScroll.ViewportWidth || point.Y > this.ItemsScroll.ViewportHeight || point.X < 0.0 || point.Y < 0.0)
					{
						this.ItemsScroll.ScrollToHorizontalOffset(this.ItemsScroll.HorizontalOffset + point.X);
						this.ItemsScroll.ScrollToVerticalOffset(this.ItemsScroll.VerticalOffset + point.Y);
					}
				}
			}
		}
		private void ReportButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationContext.NavigateToReport();
		}

        private void AdminButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationContext.NavigateToViewModel(new LoginViewModel());
        }
	}
}
