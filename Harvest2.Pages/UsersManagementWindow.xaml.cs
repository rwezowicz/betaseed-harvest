using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class UsersManagementWindow : Window, IComponentConnector, IStyleConnector
	{
		private UsersManagementViewModel _model;
		public UsersManagementWindow(UsersManagementViewModel model)
		{
			this.InitializeComponent();
			this._model = model;
			base.DataContext = this._model;
		}
		private void RemoveButton_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			if (button != null)
			{
				UserViewModel userViewModel = button.DataContext as UserViewModel;
				if (userViewModel == null)
				{
					return;
				}
				MessageBoxResult messageBoxResult = MessageBox.Show("Do you really want to remove this user and all connected data?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (messageBoxResult == MessageBoxResult.Yes)
				{
					this._model.DeleteUser(userViewModel);
				}
			}
		}
		private void AddButton_Click(object sender, RoutedEventArgs e)
		{
			if (this._model.AddUser(this.AddLoginBox.Text.Trim(), this.AddPasswordBox.Password.Trim()))
			{
				this.AddLoginBox.Text = (this.AddPasswordBox.Password = string.Empty);
				this.AddLoginBox.Focus();
			}
		}
		private void AddUser_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				this.AddButton_Click(null, new RoutedEventArgs());
			}
		}
		private void EditButton_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			if (button != null)
			{
				UserViewModel userViewModel = button.DataContext as UserViewModel;
				if (userViewModel == null)
				{
					return;
				}
				userViewModel.IsInEditMode = true;
			}
		}
		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			FrameworkElement frameworkElement = sender as FrameworkElement;
			if (frameworkElement != null)
			{
				UserViewModel userViewModel = frameworkElement.DataContext as UserViewModel;
				if (userViewModel == null)
				{
					return;
				}
				if (this._model.ValidateUserData(userViewModel.Login, userViewModel.Password))
				{
					if (userViewModel.Save())
					{
						userViewModel.IsInEditMode = false;
						return;
					}
					this._model.ValidationErrors.Add("User with the same login already exists");
					this._model.NotifyErrorChanged();
				}
			}
		}
		private void SaveUser_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				this.SaveButton_Click(sender, new RoutedEventArgs());
			}
		}
		private void ManageUsersWindow_Closing(object sender, CancelEventArgs e)
		{
			if (this._model.HasUnsavedData() && MessageBox.Show("There are unsaved users data. Do you really want to exit without saving?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
			{
				e.Cancel = true;
			}
		}
	}
}
