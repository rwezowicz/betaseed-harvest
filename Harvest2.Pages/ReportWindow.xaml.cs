using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class ReportWindow : Window, IComponentConnector
	{
		private List<PlotDetailsViewModel> _plots;
		public ReportWindow(List<PlotDetailsViewModel> plots)
		{
			this.InitializeComponent();
			this._plots = plots;
			this.PlotsList.ItemsSource = 
				from it in plots
				orderby it.HarvestOrder
				select it;
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/reportwindow.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    if (connectionId == 1)
		//    {
		//        this.PlotsList = (ItemsControl)target;
		//        return;
		//    }
		//    this._contentLoaded = true;
		//}
	}
}
