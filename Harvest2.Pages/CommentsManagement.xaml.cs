using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class CommentsManagement : Window, IComponentConnector
	{
		private CommentsViewModel _model;
		public CommentsManagement(CommentsViewModel model)
		{
			this._model = model;
			base.DataContext = model;
			this.InitializeComponent();
		}
		private void AddComment_Click(object sender, RoutedEventArgs e)
		{
			this._model.AddComment();
		}
		private void RemoveButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.CommentsList.SelectedItems.Count == 0)
			{
				return;
			}
			MessageBoxResult messageBoxResult = MessageBox.Show("Remove selected items?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (messageBoxResult == MessageBoxResult.Yes)
			{
				foreach (object current in this.CommentsList.SelectedItems)
				{
					CommentViewModel commentViewModel = current as CommentViewModel;
					if (commentViewModel != null)
					{
						commentViewModel.Remove();
					}
				}
				this._model.RemoveEmpty();
			}
		}
		private void Close_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/commentsmanagement.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        this.CommentEdit = (TextBox)target;
		//        return;
		//    case 2:
		//        this.AddComment = (Button)target;
		//        this.AddComment.Click += new RoutedEventHandler(this.AddComment_Click);
		//        return;
		//    case 3:
		//        this.CommentsList = (ListView)target;
		//        return;
		//    case 4:
		//        this.RemoveButton = (Button)target;
		//        this.RemoveButton.Click += new RoutedEventHandler(this.RemoveButton_Click);
		//        return;
		//    case 5:
		//        this.Close = (Button)target;
		//        this.Close.Click += new RoutedEventHandler(this.Close_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
