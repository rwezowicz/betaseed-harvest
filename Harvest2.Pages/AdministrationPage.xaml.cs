using Harvest2.Helpers;
using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class AdministrationPage : Page, IComponentConnector
	{
		public AdministrationPage()
		{
			this.InitializeComponent();
		}
		private void ManageComments_Click(object sender, RoutedEventArgs e)
		{
			NavigationContext.NavigateToCommentsManagement();
		}
		private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationContext.NavigateToChangePassword();
		}
		private void LogoutButton_Click(object sender, RoutedEventArgs e)
		{
            //12/4/2014 - Send Directly Back to Plot Management
            //LoginViewModel model = new LoginViewModel();
            //NavigationContext.LogOut();
            //NavigationContext.NavigateToViewModel(model);
            NavigationContext.NavigateToPlotManagement();
		}
		private void ManageUsersButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationContext.NavigateToUsersManagement();
		}
		private void SystemSettings_Click(object sender, RoutedEventArgs e)
		{
			NavigationContext.NavigateToSystemSettings();
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/administrationpage.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        this.ChangePasswordButton = (Button)target;
		//        this.ChangePasswordButton.Click += new RoutedEventHandler(this.ChangePasswordButton_Click);
		//        return;
		//    case 2:
		//        this.ManageUsersButton = (Button)target;
		//        this.ManageUsersButton.Click += new RoutedEventHandler(this.ManageUsersButton_Click);
		//        return;
		//    case 3:
		//        this.ManageComments = (Button)target;
		//        this.ManageComments.Click += new RoutedEventHandler(this.ManageComments_Click);
		//        return;
		//    case 4:
		//        this.SystemSettings = (Button)target;
		//        this.SystemSettings.Click += new RoutedEventHandler(this.SystemSettings_Click);
		//        return;
		//    case 5:
		//        this.LogoutButton = (Button)target;
		//        this.LogoutButton.Click += new RoutedEventHandler(this.LogoutButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
