using Harvest2.Helpers;
using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
    public partial class PlotDetailsPage : Window, IComponentConnector
    {
        private PlotDetailsViewModel _model;
        private CommentsViewModel _comments;
        public PlotDetailsViewModel Model
        {
            get
            {
                return this._model;
            }
            set
            {
                if (this._model != null)
                {
                    this._model.NotifyState();
                }
                this._model = value;
                base.DataContext = value;
                this._model.NotifyState();

                //this.TrialBox.Focus();

                if (string.IsNullOrEmpty(this.Model.Comment))
                {
                    this.PredefinedCommentsBox.SelectedItem = CommentViewModel.None;
                    return;
                }
                CommentViewModel commentViewModel = this._comments.Comments.FirstOrDefault((CommentViewModel it) => it.Comment.Equals(this.Model.Comment, StringComparison.CurrentCultureIgnoreCase));
                if (commentViewModel != null)
                {
                    this.PredefinedCommentsBox.SelectedItem = commentViewModel;
                    return;
                }
                this.PredefinedCommentsBox.SelectedItem = CommentViewModel.Other;
            }
        }
        public PlotDetailsPage(CommentsViewModel comments)
        {
            this._comments = comments;
            this.InitializeComponent();
            this.PredefinedCommentsBox.ItemsSource = this._comments.Comments;
            PssSHopProcessor.ProcessorInstance.ProcessingStateChangedEvent += new Action(this.ProcessorInstance_ProcessingStateChanged);
        }
        private void ProcessorInstance_ProcessingStateChanged()
        {
            if (this._model != null)
            {
                this._model.NotifyIsEnabled();
            }
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.ProcessingLabel.Content = "Processing plot. Please wait";
            this.Save(delegate
            {
                base.Dispatcher.Invoke((Action)delegate
                {
                    NavigationContext.NavigateToPlotManagement();
                    this.Model.NotifyState();
                    base.Close();
                }, new object[0]);
            });
        }

        private void SaveNoProcessButton_Click(object sender, RoutedEventArgs e)
        {
            this.ProcessingLabel.Content = "Processing plot. Please wait";
            this.Save(delegate
            {
                base.Dispatcher.Invoke((Action)delegate
                {
                    NavigationContext.NavigateToPlotManagement();
                    this.Model.NotifyState();
                    base.Close();
                }, new object[0]);
            }, ControlProcessing.DontProcess);
        }

        /// <summary>
        /// Control Processing Settings
        /// </summary>
        private enum ControlProcessing { Process, DontProcess}

        private bool Save(Action onSaved, ControlProcessing noProcessing = ControlProcessing.Process)
        {
            if (this.PredefinedCommentsBox.SelectedItem == null || this.PredefinedCommentsBox.SelectedItem == CommentViewModel.None)
            {
                this.Model.Comment = string.Empty;
            }
            else
            {
                if (this.PredefinedCommentsBox.SelectedItem != CommentViewModel.Other)
                {
                    this.Model.Comment = (this.PredefinedCommentsBox.SelectedItem as CommentViewModel).Comment;
                }
            }
            if (noProcessing == ControlProcessing.DontProcess)
            {
                return this.Model.SaveWithoutProcessing(onSaved);
            }
            else
            {
                return this.Model.Save(onSaved);
            }
        }
        private void WeightButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationContext.Logger.Info("Weight button clicked");
            this.WeightButton.IsEnabled = false;
            this.WeightButton.Content = "Reading weight...";
            this.Model.ReadWeight();
            this.WeightButton.Content = "Weight Now";
            this.WeightButton.IsEnabled = true;
        }
        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            PlotDetailsViewModel plotDetailsViewModel = NavigationContext.GotoPreviousPlot();
            if (plotDetailsViewModel != null)
            {
                this.Model = plotDetailsViewModel;
            }
        }
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Save(null))
            {
                PlotDetailsViewModel plotDetailsViewModel = NavigationContext.GotoNextPlot();
                if (plotDetailsViewModel != null)
                {
                    this.Model = plotDetailsViewModel;
                }
            }
        }
        private void ResumeButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Save(null))
            {
                PlotDetailsViewModel plotDetailsViewModel = NavigationContext.GotoNextNotEnteredPlot();
                if (plotDetailsViewModel != null)
                {
                    this.Model = plotDetailsViewModel;
                }
            }
        }
        private void SkipPlot_Click(object sender, RoutedEventArgs e)
        {
            PlotDetailsViewModel plotDetailsViewModel = NavigationContext.GotoNextPlot();
            this.Model.Refresh();
            if (plotDetailsViewModel != null)
            {
                this.Model = plotDetailsViewModel;
            }
        }
        private void PredefinedCommentsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.PredefinedCommentsBox.SelectedItem == CommentViewModel.Other)
            {
                this.NotesBox.Visibility = Visibility.Visible;
                if (e.RemovedItems.Count > 0)
                {
                    this.Model.Comment = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.PredefinedCommentsBox.SelectedItem == CommentViewModel.None)
                {
                    this.NotesBox.Visibility = Visibility.Hidden;
                    this.Model.Comment = string.Empty;
                    return;
                }
                this.NotesBox.Visibility = Visibility.Hidden;
                this.Model.Comment = (this.PredefinedCommentsBox.SelectedItem as CommentViewModel).Comment;
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                PssSHopProcessor.ProcessorInstance.ProcessingStateChangedEvent -= new Action(this.ProcessorInstance_ProcessingStateChanged);
            }
            catch
            {
            }
            NavigationContext.NavigateToPlotManagement();
            this.Model.Refresh();
            this.Model.NotifyState();
        }
        private void RootRot_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                if (!string.IsNullOrEmpty(this.RootRot.Text))
                {
                    int num = Convert.ToInt32(this.RootRot.Text);
                    if (num > 1 || (num == 1 && e.Key > Key.D0))
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }
        private void WeightBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal || e.Key == Key.OemPeriod || e.Key == Key.OemComma)
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
        }
        //[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
        //public void InitializeComponent()
        //{
        //    if (this._contentLoaded)
        //    {
        //        return;
        //    }
        //    this._contentLoaded = true;
        //    Uri resourceLocator = new Uri("/Harvest2;component/pages/plotdetailspage.xaml", UriKind.Relative);
        //    Application.LoadComponent(this, resourceLocator);
        //}
        //[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
        //void IComponentConnector.Connect(int connectionId, object target)
        //{
        //    switch (connectionId)
        //    {
        //        case 1:
        //            ((PlotDetailsPage)target).Closed += new EventHandler(this.Window_Closed);
        //            return;
        //        case 2:
        //            this.ErrorLable = (Label)target;
        //            return;
        //        case 3:
        //            this.ProcessingLabel = (Label)target;
        //            return;
        //        case 4:
        //            this.PlotNumber = (Label)target;
        //            return;
        //        case 5:
        //            this.TrialBox = (TextBlock)target;
        //            return;
        //        case 6:
        //            this.Range = (TextBlock)target;
        //            return;
        //        case 7:
        //            this.Row = (TextBlock)target;
        //            return;
        //        case 8:
        //            this.HarvestOrder = (TextBlock)target;
        //            return;
        //        case 9:
        //            this.WeightBox = (TextBox)target;
        //            this.WeightBox.KeyDown += new KeyEventHandler(this.WeightBox_KeyDown);
        //            return;
        //        case 10:
        //            this.WeightButton = (Button)target;
        //            this.WeightButton.Click += new RoutedEventHandler(this.WeightButton_Click);
        //            return;
        //        case 11:
        //            this.RootRot = (ComboBox)target;
        //            return;
        //        case 12:
        //            this.PredefinedCommentsBox = (ComboBox)target;
        //            this.PredefinedCommentsBox.SelectionChanged += new SelectionChangedEventHandler(this.PredefinedCommentsBox_SelectionChanged);
        //            return;
        //        case 13:
        //            this.NotesBox = (TextBox)target;
        //            return;
        //        case 14:
        //            this.SaveButton = (Button)target;
        //            this.SaveButton.Click += new RoutedEventHandler(this.SaveButton_Click);
        //            return;
        //        case 15:
        //            this.PreviousButton = (Button)target;
        //            this.PreviousButton.Click += new RoutedEventHandler(this.PreviousButton_Click);
        //            return;
        //        case 16:
        //            this.SkipPlot = (Button)target;
        //            this.SkipPlot.Click += new RoutedEventHandler(this.SkipPlot_Click);
        //            return;
        //        case 17:
        //            this.ResumeButton = (Button)target;
        //            this.ResumeButton.Click += new RoutedEventHandler(this.ResumeButton_Click);
        //            return;
        //        case 18:
        //            this.NextButton = (Button)target;
        //            this.NextButton.Click += new RoutedEventHandler(this.NextButton_Click);
        //            return;
        //        default:
        //            this._contentLoaded = true;
        //            return;
        //    }
        //}
    }
}
