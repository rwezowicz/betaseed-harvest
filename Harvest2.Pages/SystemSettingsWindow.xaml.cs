using Harvest2.Helpers;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class SystemSettingsWindow : Window, IComponentConnector
	{
		public SystemSettingsWindow()
		{
			this.InitializeComponent();
			this.LogisticFolderPath.Text = Confiruration.LogisticFolderPath;
			this.LogisticResponseFolderPath.Text = Confiruration.LogisticResponsePath;
			this.InitSerialPortSettings();
		}
		private void InitSerialPortSettings()
		{
			ComPortConfiguration comSettings = Confiruration.ComPortConfiguration;
			this.UseComBox.IsChecked = new bool?(comSettings.UseComPort);
			this.PortSelectBox.ItemsSource = SerialPortReader.GetPostNames();
			this.PortSelectBox.SelectedItem = comSettings.PortName;
			this.BaudRateSelectBox.ItemsSource = SerialPortReader.BaudRates;
			this.BaudRateSelectBox.SelectedItem = comSettings.BaudRate;
			this.DataBitsSelectBox.ItemsSource = SerialPortReader.DataBits;
			this.DataBitsSelectBox.SelectedItem = comSettings.DataBits;
			this.ParitySelectBox.ItemsSource = SerialPortReader.AvailableParity;
			this.ParitySelectBox.SelectedItem = SerialPortReader.AvailableParity.FirstOrDefault((KeyValuePair<Parity, string> it) => it.Key == comSettings.Parity);
			this.StopBitsBox.ItemsSource = SerialPortReader.AvailableStopBits;
			this.StopBitsBox.SelectedItem = SerialPortReader.AvailableStopBits.FirstOrDefault((KeyValuePair<StopBits, string> it) => it.Key == comSettings.StopBit);
			this.SetComPortControlsState(this.UseComBox.IsChecked.HasValue && this.UseComBox.IsChecked.Value);
		}
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			base.Close();
		}
		private void BrowseLogisticFolder_Click(object sender, RoutedEventArgs e)
		{
			string text = this.SelectFolder(this.LogisticFolderPath.Text);
			if (!string.IsNullOrEmpty(text))
			{
				this.LogisticFolderPath.Text = text;
			}
		}
		private string SelectFolder(string initialFolder)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.SelectedPath = initialFolder;
			folderBrowserDialog.ShowNewFolderButton = true;
			if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				return folderBrowserDialog.SelectedPath;
			}
			return string.Empty;
		}
		private void BrowseLogisticResponseFolder_Click(object sender, RoutedEventArgs e)
		{
			string text = this.SelectFolder(this.LogisticResponseFolderPath.Text);
			if (!string.IsNullOrEmpty(text))
			{
				this.LogisticResponseFolderPath.Text = text;
			}
		}
		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			Confiruration.SetLogisticSettings(this.LogisticFolderPath.Text, this.LogisticResponseFolderPath.Text);
			ComPortConfiguration comPortConfiguration = Confiruration.ComPortConfiguration;
			comPortConfiguration.UseComPort = (this.UseComBox.IsChecked.HasValue && this.UseComBox.IsChecked.Value);
			if (this.PortSelectBox.SelectedItem != null)
			{
				comPortConfiguration.PortName = this.PortSelectBox.SelectedItem.ToString();
			}
			if (this.BaudRateSelectBox.SelectedItem != null)
			{
				comPortConfiguration.BaudRate = (int)this.BaudRateSelectBox.SelectedItem;
			}
			if (this.DataBitsSelectBox.SelectedItem != null)
			{
				comPortConfiguration.DataBits = (int)this.DataBitsSelectBox.SelectedItem;
			}
			if (this.ParitySelectBox.SelectedItem != null)
			{
				comPortConfiguration.Parity = ((KeyValuePair<Parity, string>)this.ParitySelectBox.SelectedItem).Key;
			}
			if (this.StopBitsBox.SelectedItem != null)
			{
				comPortConfiguration.StopBit = ((KeyValuePair<StopBits, string>)this.StopBitsBox.SelectedItem).Key;
			}
			comPortConfiguration.WriteComSettings();
			base.Close();
		}
		private void SetComPortControlsState(bool isEnabled)
		{
			UIElement arg_3F_0 = this.PortSelectBox;
			UIElement arg_39_0 = this.BaudRateSelectBox;
			UIElement arg_31_0 = this.DataBitsSelectBox;
			UIElement arg_29_0 = this.ParitySelectBox;
			this.StopBitsBox.IsEnabled = isEnabled;
			arg_29_0.IsEnabled = isEnabled;
			arg_31_0.IsEnabled = isEnabled;
			arg_39_0.IsEnabled = isEnabled;
			arg_3F_0.IsEnabled = isEnabled;
		}
		private void UseComBox_Checked(object sender, RoutedEventArgs e)
		{
			this.SetComPortControlsState(this.UseComBox.IsChecked.HasValue && this.UseComBox.IsChecked.Value);
		}
		private void Window_Closed(object sender, EventArgs e)
		{
			if (App.SettingsEditMode)
			{
				System.Windows.Application.Current.Shutdown();
			}
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/systemsettingswindow.xaml", UriKind.Relative);
		//    System.Windows.Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        ((SystemSettingsWindow)target).Closed += new EventHandler(this.Window_Closed);
		//        return;
		//    case 2:
		//        this.BrowseLogisticFolder = (System.Windows.Controls.Button)target;
		//        this.BrowseLogisticFolder.Click += new RoutedEventHandler(this.BrowseLogisticFolder_Click);
		//        return;
		//    case 3:
		//        this.LogisticFolderPath = (System.Windows.Controls.TextBox)target;
		//        return;
		//    case 4:
		//        this.BrowseLogisticResponseFolder = (System.Windows.Controls.Button)target;
		//        this.BrowseLogisticResponseFolder.Click += new RoutedEventHandler(this.BrowseLogisticResponseFolder_Click);
		//        return;
		//    case 5:
		//        this.LogisticResponseFolderPath = (System.Windows.Controls.TextBox)target;
		//        return;
		//    case 6:
		//        this.UseComBox = (System.Windows.Controls.CheckBox)target;
		//        this.UseComBox.Checked += new RoutedEventHandler(this.UseComBox_Checked);
		//        this.UseComBox.Unchecked += new RoutedEventHandler(this.UseComBox_Checked);
		//        return;
		//    case 7:
		//        this.PortSelectBox = (System.Windows.Controls.ComboBox)target;
		//        return;
		//    case 8:
		//        this.BaudRateSelectBox = (System.Windows.Controls.ComboBox)target;
		//        return;
		//    case 9:
		//        this.DataBitsSelectBox = (System.Windows.Controls.ComboBox)target;
		//        return;
		//    case 10:
		//        this.ParitySelectBox = (System.Windows.Controls.ComboBox)target;
		//        return;
		//    case 11:
		//        this.StopBitsBox = (System.Windows.Controls.ComboBox)target;
		//        return;
		//    case 12:
		//        this.SaveButton = (System.Windows.Controls.Button)target;
		//        this.SaveButton.Click += new RoutedEventHandler(this.SaveButton_Click);
		//        return;
		//    case 13:
		//        this.CancelButton = (System.Windows.Controls.Button)target;
		//        this.CancelButton.Click += new RoutedEventHandler(this.CancelButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
