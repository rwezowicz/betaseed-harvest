using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
namespace Harvest2.Pages
{
	public partial class LoginPage : Page, IComponentConnector
	{
		private LoginViewModel _model;
		public LoginViewModel Model
		{
			get
			{
				return this._model;
			}
			set
			{
				this._model = value;
				base.DataContext = this._model;
			}
		}
		public LoginPage(LoginViewModel model)
		{
			this.Model = model;
			this.InitializeComponent();
			this.LoginBox.Focus();
		}
		private void LoginButton_Click(object sender, RoutedEventArgs e)
		{
			this.Model.Password = this.PasswordBox.Password;
			this.Model.SignIn();
		}
		private void LoginPage_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				this.LoginButton_Click(sender, null);
			}
		}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), DebuggerNonUserCode]
		//public void InitializeComponent()
		//{
		//    if (this._contentLoaded)
		//    {
		//        return;
		//    }
		//    this._contentLoaded = true;
		//    Uri resourceLocator = new Uri("/Harvest2;component/pages/loginpage.xaml", UriKind.Relative);
		//    Application.LoadComponent(this, resourceLocator);
		//}
		//[GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never), DebuggerNonUserCode]
		//void IComponentConnector.Connect(int connectionId, object target)
		//{
		//    switch (connectionId)
		//    {
		//    case 1:
		//        ((LoginPage)target).KeyDown += new KeyEventHandler(this.LoginPage_KeyDown);
		//        return;
		//    case 2:
		//        this.ErrorLable = (Label)target;
		//        return;
		//    case 3:
		//        this.FormHeader = (Label)target;
		//        return;
		//    case 4:
		//        this.LoginLabel = (Label)target;
		//        return;
		//    case 5:
		//        this.LoginBox = (TextBox)target;
		//        return;
		//    case 6:
		//        this.PasswordLabel = (Label)target;
		//        return;
		//    case 7:
		//        this.PasswordBox = (PasswordBox)target;
		//        return;
		//    case 8:
		//        this.LoginButton = (Button)target;
		//        this.LoginButton.Click += new RoutedEventHandler(this.LoginButton_Click);
		//        return;
		//    default:
		//        this._contentLoaded = true;
		//        return;
		//    }
		//}
	}
}
