using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
namespace Harvest2.Properties
{
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0"), CompilerGenerated]
	internal sealed class Settings : ApplicationSettingsBase
	{
		private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}
		[DefaultSettingValue("Logistic"), UserScopedSetting, DebuggerNonUserCode]
		public string LogisticDirectory
		{
			get
			{
				return (string)this["LogisticDirectory"];
			}
			set
			{
				this["LogisticDirectory"] = value;
			}
		}
		[DefaultSettingValue("Logistic"), UserScopedSetting, DebuggerNonUserCode]
		public string LogisticResponseDirectory
		{
			get
			{
				return (string)this["LogisticResponseDirectory"];
			}
			set
			{
				this["LogisticResponseDirectory"] = value;
			}
		}
		[DefaultSettingValue("20"), UserScopedSetting, DebuggerNonUserCode]
		public int MaxSecondsToGetLogisticResponse
		{
			get
			{
				return (int)this["MaxSecondsToGetLogisticResponse"];
			}
			set
			{
				this["MaxSecondsToGetLogisticResponse"] = value;
			}
		}
		[DefaultSettingValue("True"), UserScopedSetting, DebuggerNonUserCode]
		public bool COM_UseCom
		{
			get
			{
				return (bool)this["COM_UseCom"];
			}
			set
			{
				this["COM_UseCom"] = value;
			}
		}
		[DefaultSettingValue(""), UserScopedSetting, DebuggerNonUserCode]
		public string COM_PortName
		{
			get
			{
				return (string)this["COM_PortName"];
			}
			set
			{
				this["COM_PortName"] = value;
			}
		}
		[DefaultSettingValue("57600"), UserScopedSetting, DebuggerNonUserCode]
		public int COM_BaudRate
		{
			get
			{
				return (int)this["COM_BaudRate"];
			}
			set
			{
				this["COM_BaudRate"] = value;
			}
		}
		[DefaultSettingValue("8"), UserScopedSetting, DebuggerNonUserCode]
		public int COM_DataBits
		{
			get
			{
				return (int)this["COM_DataBits"];
			}
			set
			{
				this["COM_DataBits"] = value;
			}
		}
		[DefaultSettingValue("0"), UserScopedSetting, DebuggerNonUserCode]
		public int COM_Parity
		{
			get
			{
				return (int)this["COM_Parity"];
			}
			set
			{
				this["COM_Parity"] = value;
			}
		}
		[DefaultSettingValue("0"), UserScopedSetting, DebuggerNonUserCode]
		public int COM_StopBits
		{
			get
			{
				return (int)this["COM_StopBits"];
			}
			set
			{
				this["COM_StopBits"] = value;
			}
		}
		[ApplicationScopedSetting, DefaultSettingValue("Data Source=|DataDirectory|Harvest2\\Db.sdf;Persist Security Info=False;"), DebuggerNonUserCode]
		public string ConnectionString
		{
			get
			{
				return (string)this["ConnectionString"];
			}
		}
	}
}
