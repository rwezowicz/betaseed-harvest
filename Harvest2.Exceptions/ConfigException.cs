using System;
namespace Harvest2.Exceptions
{
	public class ConfigException : Exception
	{
		public ConfigException()
		{
		}
		public ConfigException(string message) : base(message)
		{
		}
	}
}
