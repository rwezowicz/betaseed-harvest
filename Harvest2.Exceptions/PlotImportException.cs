using System;
namespace Harvest2.Exceptions
{
	public class PlotImportException : Exception
	{
		public PlotImportException()
		{
		}
		public PlotImportException(string message) : base(message)
		{
		}
	}
}
