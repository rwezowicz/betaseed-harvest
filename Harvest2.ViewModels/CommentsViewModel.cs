using Harvest2.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
namespace Harvest2.ViewModels
{
	public class CommentsViewModel : ViewModelBase
	{
		private ObservableCollection<CommentViewModel> _comments;
		private string _editingValue;
		public ObservableCollection<CommentViewModel> Comments
		{
			get
			{
				return this._comments;
			}
			set
			{
				this._comments = value;
				base.RisePropertyChange("Comments");
				base.RisePropertyChange("HasComments");
			}
		}
		public string EditingValue
		{
			get
			{
				return this._editingValue;
			}
			set
			{
				this._editingValue = value;
				base.RisePropertyChange("EditingValue");
				base.RisePropertyChange("AddingEnable");
			}
		}
		public bool AddingEnable
		{
			get
			{
				return !string.IsNullOrEmpty(this.EditingValue);
			}
		}
		public bool HasComments
		{
			get
			{
				return this._comments.Count > 0;
			}
		}
		public CommentsViewModel(bool addSystemItems = false)
		{
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				IOrderedQueryable<PredefinedComment> source = 
					from it in dataContext.PredefinedComments
					orderby it.Comment
					select it;
				this.Comments = new ObservableCollection<CommentViewModel>(
					from it in source
					select new CommentViewModel(it));
				this.Comments.CollectionChanged += new NotifyCollectionChangedEventHandler(this.Comments_CollectionChanged);
				if (addSystemItems)
				{
					this.Comments.Insert(0, CommentViewModel.None);
					this.Comments.Insert(1, CommentViewModel.Other);
				}
			}
		}
		private void Comments_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			base.RisePropertyChange("HasComments");
		}
		public void AddComment()
		{
			if (string.IsNullOrEmpty(this._editingValue))
			{
				return;
			}
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				PredefinedComment predefinedComment = new PredefinedComment();
				predefinedComment.Comment = this._editingValue;
				dataContext.PredefinedComments.InsertOnSubmit(predefinedComment);
				dataContext.SubmitChanges();
				this._comments.Add(new CommentViewModel(predefinedComment));
			}
			this.EditingValue = string.Empty;
		}
		public void RemoveEmpty()
		{
			CommentViewModel[] array = (
				from it in this._comments
				where it.IsEmpty
				select it).ToArray<CommentViewModel>();
			CommentViewModel[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				CommentViewModel item = array2[i];
				this._comments.Remove(item);
			}
		}
	}
}
