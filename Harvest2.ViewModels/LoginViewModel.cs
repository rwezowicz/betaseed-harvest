using Harvest2.Helpers;
using System;
using System.Linq;
namespace Harvest2.ViewModels
{
	public class LoginViewModel : ViewModelBase
	{
		private string _login;
		private string _password;
		public string Login
		{
			get
			{
				return this._login;
			}
			set
			{
				this._login = value;
				base.RisePropertyChange("Login");
			}
		}
		public string Password
		{
			get
			{
				return this._password;
			}
			set
			{
				this._password = value;
				base.RisePropertyChange("Password");
			}
		}
		public override bool ValidateModel()
		{
			base.ValidateModel();
			if (string.IsNullOrEmpty(this.Login) && string.IsNullOrEmpty(this.Password))
			{
				this._validationErrors.Add("Please enter login and password");
			}
			else
			{
				if (string.IsNullOrEmpty(this.Login))
				{
					this._validationErrors.Add("Please enter login");
				}
				else
				{
					if (string.IsNullOrEmpty(this.Password))
					{
						this._validationErrors.Add("Please enter password");
					}
				}
			}
			return this._validationErrors.Count == 0;
		}
		public void SignIn()
		{
			if (!this.ValidateModel())
			{
				return;
			}
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				User user = dataContext.Users.FirstOrDefault((User it) => it.Login == this.Login.ToLower() && it.Password == this.Password);
				if (user == null)
				{
					this._validationErrors.Add("Invalid login or password");
					base.NotifyErrorChanged();
				}
				else
				{
					NavigationContext.SetLoginUser(user);
					if (user.HasAdminRights)
					{
						NavigationContext.NavigateToAdminPage();
					}
					else
					{
						NavigationContext.NavigateToPlotManagement();
					}
				}
			}
		}
	}
}
