using Harvest2.Helpers;
using Harvest2.Models;
using System;
using System.Linq;
using System.Windows;
namespace Harvest2.ViewModels
{
    public class PlotDetailsViewModel : ViewModelBase
    {
        private Plot _plotDbModel;
        private readonly PlotManagementViewModel _plotManagement;
        public int PlotNumber
        {
            get
            {
                return this._plotDbModel.PlotNumber;
            }
        }
        public string PlotID
        {
            get
            {
                return this._plotDbModel.PlotId;
            }
        }
        public int HarvestOrder
        {
            get
            {
                return this._plotDbModel.HarvestOrder;
            }
        }
        public int Trial
        {
            get
            {
                return this._plotDbModel.Trial;
            }
        }
        public int Row
        {
            get
            {
                return this._plotDbModel.Row;
            }
        }
        public int Column
        {
            get
            {
                return this._plotDbModel.Column;
            }
        }
        public int Crop
        {
            get
            {
                return this._plotDbModel.Crop;
            }
        }
        public int GridRow
        {
            get
            {
                return this._plotManagement.GridRowsCount - this.Column;
            }
        }
        public int GridColumn
        {
            get
            {
                if (this._plotManagement.PlotDirection == PlotDirections.Clock)
                {
                    return this.Row - 1;
                }
                return this._plotManagement.GridColumnsCount - this.Row;
            }
        }
        public int RootRot
        {
            get
            {
                return (int)this._plotDbModel.RootRot;
            }
            set
            {
                this._plotDbModel.RootRot = (byte)value;
                base.RisePropertyChange("RootRow");
            }
        }
        public decimal Weight
        {
            get
            {
                return this._plotDbModel.Weight;
            }
            set
            {
                this._plotDbModel.Weight = value;
                base.RisePropertyChange("Weight");
            }
        }
        public string Comment
        {
            get
            {
                return this._plotDbModel.Comment;
            }
            set
            {
                this._plotDbModel.Comment = value;
                base.RisePropertyChange("Comment");
            }
        }
        public PlotStates PlotState
        {
            get
            {
                if (NavigationContext.Model == this)
                {
                    return PlotStates.InEditing;
                }
                if (this._plotDbModel.Weight <= 0m)
                {
                    if (!(NavigationContext.Model is PlotDetailsViewModel) && this.HarvestOrder == 1)
                    {
                        return PlotStates.InEditing;
                    }
                    return PlotStates.NotEntered;
                }
                else
                {
                    if (this._plotDbModel.SavedWithErrors)
                    {
                        return PlotStates.FinishedWithErrors;
                    }
                    return PlotStates.Finished;
                }
            }
        }
        public int Year
        {
            get
            {
                return this._plotDbModel.Year;
            }
        }
        public bool IsEmpty
        {
            get
            {
                return this._plotDbModel == null;
            }
        }
        public Visibility StarVisible
        {
            get
            {
                if (this.PlotState == PlotStates.NotEntered || string.IsNullOrEmpty(this.Comment))
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }
        public bool IsEnabled
        {
            get
            {
                return !PssSHopProcessor.ProcessorInstance.IsProcessing;
            }
        }
        public bool IsReadWeightEnable
        {
            get
            {
                return this.IsEnabled && Confiruration.ComPortConfiguration.UseComPort;
            }
        }
        public Plot DbPlot
        {
            get
            {
                return this._plotDbModel;
            }
        }
        public Visibility ProcessingMessageVisibility
        {
            get
            {
                if (!PssSHopProcessor.ProcessorInstance.IsProcessing)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }
        public PlotDetailsViewModel(Plot plot, PlotManagementViewModel plotManagement)
        {
            this._plotDbModel = plot;
            this._plotManagement = plotManagement;
        }
        public void NotifyState()
        {
            base.RisePropertyChange("PlotState");
            base.RisePropertyChange("StarVisible");
            this._modelChanged = false;
        }
        public void NotifyIsEnabled()
        {
            base.RisePropertyChange("IsEnabled");
            base.RisePropertyChange("IsReadWeightEnable");
            base.RisePropertyChange("ProcessingMessageVisibility");
        }
        public void Refresh()
        {
            using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
            {
                this._plotDbModel = dataContext.Plots.FirstOrDefault((Plot it) => it.Id == this._plotDbModel.Id);
                base.ValidationErrors.Clear();
                if (this.PlotState == PlotStates.FinishedWithErrors)
                {
                    base.ValidationErrors.Add("Plot was not correctly processed with Pss-S-Hop system");
                }
                base.RisePropertyChange("RootRot");
                base.RisePropertyChange("Weight");
                base.RisePropertyChange("Comment");
                this.NotifyState();
                base.NotifyErrorChanged();
            }
        }
        public void ReadWeight()
        {
            try
            {
                using (SerialPortReader serialPortReader = new SerialPortReader())
                {
                    this.Weight = serialPortReader.ReadWeight();
                }
            }
            catch (Exception message)
            {
                NavigationContext.Logger.Error(message);
            }
        }
        public override bool ValidateModel()
        {
            base.ValidateModel();
            if (this.Weight <= 0m)
            {
                base.ValidationErrors.Add("Weight should be specified");
            }
            if (this.RootRot < 0 || this.RootRot > 10)
            {
                base.ValidationErrors.Add("Invalid Root Rot value. Should be in range [0;10]");
            }
            base.NotifyErrorChanged();
            return base.ValidationErrors.Count == 0;
        }
        public bool Save(Action onSaved)
        {
            if (this.ValidateModel())
            {
                PssSHopProcessor.ProcessorInstance.StartPssSHopProcessing(this._plotDbModel, delegate(Plot plot, bool hasErrors)
                {
                    using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
                    {
                        Plot plot2 = dataContext.Plots.First((Plot it) => it.Id == plot.Id);
                        plot2.Comment = plot.Comment;
                        plot2.RootRot = plot.RootRot;
                        plot2.Weight = plot.Weight;
                        Plot arg_D7_0 = plot2;
                        plot.SavedWithErrors = hasErrors;
                        arg_D7_0.SavedWithErrors = hasErrors;
                        this.NotifyState();
                        dataContext.SubmitChanges();
                        if (onSaved != null)
                        {
                            onSaved();
                        }
                    }
                });
                return true;
            }
            return false;
        }

        public bool SaveWithoutProcessing(Action onSaved)
        {
            if (this.ValidateModel())
            {
                Plot plot = this._plotDbModel;
                using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
                {
                    Plot plot2 = dataContext.Plots.First((Plot it) => it.Id == plot.Id);
                    plot2.Comment = plot.Comment;
                    plot2.RootRot = plot.RootRot;
                    plot2.Weight = plot.Weight;
                    Plot arg_D7_0 = plot2;
                    plot.SavedWithErrors = false;
                    arg_D7_0.SavedWithErrors = false;
                    this.NotifyState();
                    dataContext.SubmitChanges();
                    if (onSaved != null)
                    {
                        onSaved();
                    }
                }
                return true;
            }
            return false;
        }
    }
}
