using Harvest2.Helpers;
using System;
using System.Linq;
namespace Harvest2.ViewModels
{
	public class ChangePasswordViewModel : ViewModelBase
	{
		private string _oldPassword;
		private string _newPassword;
		private string _confirmPassword;
		public string OldPassword
		{
			get
			{
				return this._oldPassword;
			}
			set
			{
				this._oldPassword = value;
				base.RisePropertyChange("OldPassword");
			}
		}
		public string NewPassword
		{
			get
			{
				return this._newPassword;
			}
			set
			{
				this._newPassword = value;
				base.RisePropertyChange("NewPassword");
			}
		}
		public string ConfirmPassword
		{
			get
			{
				return this._confirmPassword;
			}
			set
			{
				this._confirmPassword = value;
				base.RisePropertyChange("ConfirmPassword");
			}
		}
		public override bool ValidateModel()
		{
			base.ValidateModel();
			if (string.IsNullOrEmpty(this.OldPassword) || string.IsNullOrEmpty(this.NewPassword) || string.IsNullOrEmpty(this.ConfirmPassword))
			{
				base.ValidationErrors.Add("All fields must be completed");
			}
			else
			{
				if (this.NewPassword != this.ConfirmPassword)
				{
					base.ValidationErrors.Add("New password and confirmation must be equal");
				}
				else
				{
					if (!this.ValidateUserPassword())
					{
						base.ValidationErrors.Add("Wrong current password");
					}
				}
			}
			base.NotifyErrorChanged();
			return base.ValidationErrors.Count == 0;
		}
		private bool ValidateUserPassword()
		{
			bool result;
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				result = dataContext.Users.Any((User it) => it.Id == NavigationContext.UserData.Id && it.Password == this.OldPassword);
			}
			return result;
		}
		public void SetNewPassword()
		{
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				User user = dataContext.Users.First((User it) => it.Id == NavigationContext.UserData.Id);
				user.Password = this.NewPassword;
				dataContext.SubmitChanges();
				NavigationContext.SetLoginUser(user);
			}
		}
	}
}
