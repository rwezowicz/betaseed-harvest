using Harvest2.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
namespace Harvest2.ViewModels
{
	public class UsersManagementViewModel : ViewModelBase
	{
		private ObservableCollection<UserViewModel> _users;
		public ObservableCollection<UserViewModel> Users
		{
			get
			{
				return this._users;
			}
		}
		public UsersManagementViewModel()
		{
			this._users = new ObservableCollection<UserViewModel>();
		}
		public void DeleteUser(UserViewModel user)
		{
			user.Delete();
			this._users.Remove(user);
		}
		public static UsersManagementViewModel GetUsers()
		{
			UsersManagementViewModel usersManagementViewModel = new UsersManagementViewModel();
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				usersManagementViewModel._users = new ObservableCollection<UserViewModel>(
					from it in dataContext.Users
					where !it.HasAdminRights
					select new UserViewModel(it));
			}
			return usersManagementViewModel;
		}
		public bool AddUser(string login, string password)
		{
			if (!this.ValidateUserData(login, password))
			{
				return false;
			}
			UserViewModel userViewModel = new UserViewModel();
			userViewModel.Login = login;
			userViewModel.Password = password;
			if (userViewModel.Add())
			{
				this._users.Add(userViewModel);
				return true;
			}
			base.ValidationErrors.Add("User with the same login already exists");
			base.NotifyErrorChanged();
			return false;
		}
		public bool ValidateUserData(string login, string password)
		{
			base.ValidationErrors.Clear();
			base.NotifyErrorChanged();
			if (string.IsNullOrEmpty(login) && string.IsNullOrEmpty(password))
			{
				base.ValidationErrors.Add("To add user login and password must be set");
			}
			else
			{
				if (string.IsNullOrEmpty(login))
				{
					base.ValidationErrors.Add("Login can't be empty");
				}
				else
				{
					if (string.IsNullOrEmpty(password))
					{
						base.ValidationErrors.Add("Password can't be empty");
					}
				}
			}
			if (base.ValidationErrors.Count > 0)
			{
				base.NotifyErrorChanged();
				return false;
			}
			return true;
		}
		public bool HasUnsavedData()
		{
			return this._users.Any((UserViewModel it) => it.IsInEditMode);
		}
	}
}
