using Harvest2.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
namespace Harvest2.ViewModels
{
	public class PlotManagementViewModel : ViewModelBase
	{
		private PlotDirections _plotDirection;
		private ObservableCollection<PlotDetailsViewModel> _plots;
		private int _gridRowsCount;
		private int _gridColumnsCount;
		public ObservableCollection<PlotDetailsViewModel> Plots
		{
			get
			{
				return this._plots;
			}
			set
			{
				this._plots = value;
				base.RisePropertyChange("Plots");
			}
		}
		public int GridColumnsCount
		{
			get
			{
				return this._gridColumnsCount;
			}
			set
			{
				this._gridColumnsCount = value;
				base.RisePropertyChange("GridColumnsCount");
			}
		}
		public int GridRowsCount
		{
			get
			{
				return this._gridRowsCount;
			}
			set
			{
				this._gridRowsCount = value;
				base.RisePropertyChange("GridRowsCount");
			}
		}
		public double ScaleFactor
		{
			get;
			private set;
		}
		public PlotDirections PlotDirection
		{
			get
			{
				return this._plotDirection;
			}
			set
			{
				this._plotDirection = value;
				this.ReorderPlots(this._plotDirection);
			}
		}
		public bool HasPlots
		{
			get
			{
				return this._plots.Count > 0;
			}
		}
		public Visibility AdminButtonsVisible
		{
			get
			{
				if (!NavigationContext.UserData.HasAdminRights)
				{
					return Visibility.Collapsed;
				}
				return Visibility.Visible;
			}
		}
		public PlotManagementViewModel()
		{
			this.PlotDirection = PlotDirections.Clock;
			this._plots = new ObservableCollection<PlotDetailsViewModel>();
			this._plots.CollectionChanged += new NotifyCollectionChangedEventHandler(this._plots_CollectionChanged);
			this.ScaleFactor = 1.0;
		}
		private void _plots_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			base.RisePropertyChange("GridColumnsCount");
			base.RisePropertyChange("GridRowsCount");
			base.RisePropertyChange("HasPlots");
			this.ReorderPlots(this.PlotDirection);
		}
		public void PlotSelected(PlotDetailsViewModel plot)
		{
			if (plot != null)
			{
				NavigationContext.NavigateToViewModel(plot);
			}
			this.Plots.First<PlotDetailsViewModel>().NotifyState();
		}
		public void SetScaleTransform(double windowWidth, double windowHeight)
		{
			double num = Math.Min(windowWidth / 700.0, windowHeight / 400.0);
			this.ScaleFactor = ((num < 1.0) ? 1.0 : num);
			base.RisePropertyChange("ScaleFactor");
		}
		public void LogOut()
		{
			LoginViewModel model = new LoginViewModel();
			NavigationContext.LogOut();
			NavigationContext.NavigateToViewModel(model);
		}
		public void LoadPlots()
		{
			IEnumerable<Plot> enumerable = this.LoadAllPlots();
			if (enumerable != null && enumerable.Count<Plot>() > 0)
			{
				enumerable = 
					from it in enumerable
					where !it.IsSeparator
					select it;
				this.GridRowsCount = enumerable.Max((Plot it) => it.Column);
				this.GridColumnsCount = enumerable.Max((Plot it) => it.Row);
				this.Plots = new ObservableCollection<PlotDetailsViewModel>(
					from it in enumerable
					select new PlotDetailsViewModel(it, this));
				this.ReorderPlots(this.PlotDirection);
			}
		}
		public IEnumerable<Plot> LoadAllPlots()
		{
			IEnumerable<Plot> result;
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
                // Remove User Limitation
                // PlotGroup plotGroup = (
                //    from it in dataContext.PlotGroups
                //    orderby it.Id descending
                //    select it).FirstOrDefault((PlotGroup it) => it.User.Id == NavigationContext.UserData.Id);

				try
				{
					PlotGroup plotGroup = (
										from it in dataContext.PlotGroups
										orderby it.Id descending
										select it).First();

					if (plotGroup == null)
					{
						result = Enumerable.Empty<Plot>();
					}
					else
					{
						result = (
							from it in dataContext.Plots
							where it.PlotGroup == plotGroup
							orderby it.HarvestOrder
							select it).ToArray<Plot>();
					}
				}
				catch (Exception)
				{
					result = Enumerable.Empty<Plot>();
				}
			}
			return result;
		}
		public void ImportPlots(string filePath)
		{
			IEnumerable<Plot> enumerable = new PlotsImporter
			{
				SelectSheetAction = new Func<IEnumerable<string>, int>(this.SelectSheetOnImport)
			}.Import(filePath);
			if (enumerable == null)
			{
				return;
			}
			this.GridRowsCount = enumerable.Max((Plot it) => it.Column);
			this.GridColumnsCount = enumerable.Max((Plot it) => it.Row);
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				PlotGroup plotGroup = new PlotGroup();
				User user = dataContext.Users.First((User it) => it.Id == NavigationContext.UserData.Id);
				plotGroup.User = user;
				dataContext.PlotGroups.InsertOnSubmit(plotGroup);
				this._plots.Clear();
				foreach (Plot current in enumerable)
				{
					current.PlotGroup = plotGroup;
					dataContext.Plots.InsertOnSubmit(current);
					if (!current.IsSeparator)
					{
						this._plots.Add(new PlotDetailsViewModel(current, this));
					}
				}
				dataContext.SubmitChanges();
			}
			this.ReorderPlots(this.PlotDirection);
		}
		private int SelectSheetOnImport(IEnumerable<string> sheets)
		{
			return NavigationContext.NavigateToSelectSheetWindow(sheets);
		}
		private void ReorderPlots(PlotDirections direction)
		{
			if (this.Plots != null && this.Plots.Count > 0)
			{
				this.Plots = new ObservableCollection<PlotDetailsViewModel>(SortHelper.SortPlots(this.Plots.ToArray<PlotDetailsViewModel>(), this.PlotDirection));
			}
		}
		public void Export(string filePath)
		{
			ExportProvider exportProvider = new ExportProvider();
			exportProvider.Export(filePath, this.LoadAllPlots());
		}
	}
}
