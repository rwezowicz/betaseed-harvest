using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
namespace Harvest2.ViewModels
{
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		protected List<string> _validationErrors;
		protected bool _modelChanged;
		public event PropertyChangedEventHandler PropertyChanged;
		public IList<string> ValidationErrors
		{
			get
			{
				return this._validationErrors;
			}
		}
		public string ValidationMessage
		{
			get
			{
				if (this.ValidationErrors.Count > 0)
				{
					return string.Join(Environment.NewLine, this.ValidationErrors.ToArray<string>());
				}
				return string.Empty;
			}
		}
		public Visibility ErrorsVisibility
		{
			get
			{
				if (this._validationErrors.Count != 0)
				{
					return Visibility.Visible;
				}
				return Visibility.Hidden;
			}
		}
		public bool ModelChanged
		{
			get
			{
				return this._modelChanged;
			}
		}
		public ViewModelBase()
		{
			this._validationErrors = new List<string>();
			this._modelChanged = false;
		}
		protected void RisePropertyChange(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
			this._modelChanged = true;
		}
		public virtual bool ValidateModel()
		{
			this._validationErrors.Clear();
			this.NotifyErrorChanged();
			return true;
		}
		public void NotifyErrorChanged()
		{
			this.RisePropertyChange("ValidationMessage");
			this.RisePropertyChange("ErrorsVisibility");
		}
	}
}
