using Harvest2.Helpers;
using System;
using System.Linq;
namespace Harvest2.ViewModels
{
	public class CommentViewModel : ViewModelBase
	{
		private static CommentViewModel _none;
		private static CommentViewModel _other;
		private PredefinedComment _dbComment;
		public string Comment
		{
			get
			{
				return this._dbComment.Comment;
			}
			set
			{
				this._dbComment.Comment = value;
				base.RisePropertyChange("Comment");
			}
		}
		public bool IsEmpty
		{
			get
			{
				return this._dbComment == null;
			}
		}
		public static CommentViewModel None
		{
			get
			{
				return CommentViewModel._none;
			}
		}
		public static CommentViewModel Other
		{
			get
			{
				return CommentViewModel._other;
			}
		}
		static CommentViewModel()
		{
			CommentViewModel._none = new CommentViewModel(null);
			CommentViewModel._none._dbComment.Comment = "[None]";
			CommentViewModel._other = new CommentViewModel(null);
			CommentViewModel._other._dbComment.Comment = "[Custom]";
		}
		public CommentViewModel(PredefinedComment comment)
		{
			if (comment == null)
			{
				this._dbComment = new PredefinedComment();
				return;
			}
			this._dbComment = comment;
		}
		public void Remove()
		{
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				PredefinedComment predefinedComment = dataContext.PredefinedComments.FirstOrDefault((PredefinedComment it) => it.Id == this._dbComment.Id);
				if (predefinedComment != null)
				{
					dataContext.PredefinedComments.DeleteOnSubmit(predefinedComment);
					dataContext.SubmitChanges();
				}
			}
			this._dbComment = null;
		}
	}
}
