using Harvest2.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
namespace Harvest2.ViewModels
{
	public class UserViewModel : ViewModelBase
	{
		private User _dbUser;
		private bool _isInEditMode;
		public string Login
		{
			get
			{
				return this._dbUser.Login;
			}
			set
			{
				this._dbUser.Login = value;
				base.RisePropertyChange("Login");
			}
		}
		public string Password
		{
			get
			{
				return this._dbUser.Password;
			}
			set
			{
				this._dbUser.Password = value;
				base.RisePropertyChange("Password");
			}
		}
		public bool IsInEditMode
		{
			get
			{
				return this._isInEditMode;
			}
			set
			{
				this._isInEditMode = value;
				base.RisePropertyChange("IsInEditMode");
				base.RisePropertyChange("EditControlsVisibility");
				base.RisePropertyChange("ReadControlsVisibility");
			}
		}
		public Visibility EditControlsVisibility
		{
			get
			{
				if (!this._isInEditMode)
				{
					return Visibility.Collapsed;
				}
				return Visibility.Visible;
			}
		}
		public Visibility ReadControlsVisibility
		{
			get
			{
				if (!this._isInEditMode)
				{
					return Visibility.Visible;
				}
				return Visibility.Collapsed;
			}
		}
		public UserViewModel()
		{
			this._dbUser = new User();
		}
		public UserViewModel(User dbUser)
		{
			this._dbUser = dbUser;
		}
		public void Delete()
		{
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				IQueryable<PlotGroup> queryable = 
					from it in dataContext.PlotGroups
					where it.User.Id == this._dbUser.Id
					select it;
				using (IEnumerator<PlotGroup> enumerator = queryable.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						PlotGroup plotGroup = enumerator.Current;
						IQueryable<Plot> entities = 
							from it in dataContext.Plots
							where it.PlotGroupId == plotGroup.Id
							select it;
						dataContext.Plots.DeleteAllOnSubmit<Plot>(entities);
					}
				}
				dataContext.PlotGroups.DeleteAllOnSubmit<PlotGroup>(queryable);
				User entity = dataContext.Users.First((User it) => it.Id == this._dbUser.Id);
				dataContext.Users.DeleteOnSubmit(entity);
				dataContext.SubmitChanges();
			}
		}
		public bool Add()
		{
			bool result;
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				bool flag = dataContext.Users.Any((User it) => it.Login.ToLower() == this.Login.ToLower());
				if (flag)
				{
					result = false;
				}
				else
				{
					this._dbUser.HasAdminRights = false;
					dataContext.Users.InsertOnSubmit(this._dbUser);
					dataContext.SubmitChanges();
					result = true;
				}
			}
			return result;
		}
		public bool Save()
		{
			this.Login = this.Login.Trim();
			this.Password = this.Password.Trim();
			using (DataAccessDataContext dataContext = NavigationContext.GetDataContext())
			{
				bool flag = dataContext.Users.Any((User it) => it.Login.ToLower() == this._dbUser.Login.ToLower() && it.Id != this._dbUser.Id);
				if (flag)
				{
					return false;
				}
				User user = dataContext.Users.First((User it) => it.Id == this._dbUser.Id);
				user.Login = this._dbUser.Login;
				user.Password = this._dbUser.Password;
				dataContext.SubmitChanges();
			}
			return true;
		}
	}
}
