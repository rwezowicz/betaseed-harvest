using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
namespace Harvest2.Helpers
{
	public class PssSHopProcessor
	{
		private volatile bool _isProcessing;
		private static PssSHopProcessor _processorInstance = new PssSHopProcessor();
		public event Action ProcessingStateChangedEvent;
		public bool IsProcessing
		{
			get
			{
				return this._isProcessing;
			}
			private set
			{
				this._isProcessing = value;
				if (this.ProcessingStateChangedEvent != null)
				{
					this.ProcessingStateChangedEvent();
				}
			}
		}
		public static PssSHopProcessor ProcessorInstance
		{
			get
			{
				return PssSHopProcessor._processorInstance;
			}
		}
		public void StartPssSHopProcessing(Plot plot, Action<Plot, bool> processedAction)
		{
			if (Confiruration.SkipLogisticIntegration)
			{
				processedAction(plot, false);
				return;
			}
			string pssFileContent = this.GetPssFileContent(plot);
			string path = string.Format("{0}_{1}.plz", plot.PlotNumber, DateTime.Now.ToString("dHHmmss"));
			string filePath = Path.Combine(Confiruration.LogisticFolderPath, path);
			File.WriteAllText(filePath, pssFileContent);

			Thread thread = new Thread((ParameterizedThreadStart)delegate
			{
				try
				{
					this.IsProcessing = true;
					bool flag = this.WaitForFileProcessing(plot, filePath);
					this.IsProcessing = false;
					processedAction(plot, !flag);
				}
				finally
				{
					this.IsProcessing = false;
				}
			});
			thread.Start();
		}

		private string GetPssFileContent(Plot plot)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(plot.Crop);
			stringBuilder.Append(" ");
			stringBuilder.Append(plot.Year);
			stringBuilder.Append(" ");
			stringBuilder.Append(plot.Trial);
			stringBuilder.Append(" ");
			stringBuilder.Append(plot.Location);
			stringBuilder.Append(" ");
			stringBuilder.Append(plot.PlotNumber);
			stringBuilder.Append(" ");
			stringBuilder.Append(plot.Weight);
			stringBuilder.Append(" ");
			stringBuilder.Append(DateTime.Now.ToString("d.M.yyyy H:m"));
			return stringBuilder.ToString();
		}
		private bool WaitForFileProcessing(Plot plot, string initFilePath)
		{
			DateTime t = DateTime.Now.Add(Confiruration.LogisticResponseWait);
			string[] array;
			do
			{
				array = (
					from it in Directory.GetFiles(Confiruration.LogisticResponsePath, "*", SearchOption.TopDirectoryOnly)
					where !it.Equals(initFilePath, StringComparison.CurrentCultureIgnoreCase)
					select it).ToArray<string>();
				Thread.Sleep(TimeSpan.FromSeconds(10.0));
			}
			while (array.Length == 0 && DateTime.Now < t);
			bool result;
			if (array.Length > 0)
			{
				string path = array.First<string>();
				string text = File.ReadAllText(path);
				result = (text.Contains(string.Format(";{0};", plot.Trial)) && text.Contains(string.Format(";{0};", plot.Location)) && text.Contains(string.Format(";{0};", plot.PlotNumber)));
				File.Delete(path);
			}
			else
			{
				result = false;
			}
			File.Delete(initFilePath);
			return result;
		}
	}
}
