using Harvest2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Harvest2.Helpers
{
	internal static class SortHelper
	{
		public static IEnumerable<PlotDetailsViewModel> SortPlots(PlotDetailsViewModel[] plots, PlotDirections direction)
		{
			return 
				from it in plots
				orderby it.HarvestOrder
				select it;
		}
	}
}
