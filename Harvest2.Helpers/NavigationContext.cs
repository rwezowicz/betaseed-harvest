using Harvest2.ViewModels;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using AdministrationPage = Harvest2.Pages.AdministrationPage;
using ChangePasswordWindows = Harvest2.Pages.ChangePasswordWindows;
using CommentsManagement = Harvest2.Pages.CommentsManagement;
using LoginPage = Harvest2.Pages.LoginPage;
using PlotDetailsPage = Harvest2.Pages.PlotDetailsPage;
using PlotManagementPage = Harvest2.Pages.PlotManagementPage;
using ReportWindow = Harvest2.Pages.ReportWindow;
using SelectSheetWindow = Harvest2.Pages.SelectSheetWindow;
using SystemSettingsWindow = Harvest2.Pages.SystemSettingsWindow;
using UsersManagementWindow = Harvest2.Pages.UsersManagementWindow;

namespace Harvest2.Helpers
{
	internal class NavigationContext
	{
		private static MainWindow _mainWindow;
		private static ViewModelBase _model;
		private static User _loginData;
		private static PlotManagementViewModel _plotsData;
		private static PlotDetailsPage _detailsPage;
		private static object _lock = new object();
		public static int PlotIndex
		{
			get
			{
				if (NavigationContext._plotsData == null || NavigationContext._model == null || !(NavigationContext._model is PlotDetailsViewModel) || NavigationContext._plotsData.Plots == null || NavigationContext._plotsData.Plots.Count == 0)
				{
					return -1;
				}
				return NavigationContext._plotsData.Plots.IndexOf((PlotDetailsViewModel)NavigationContext._model);
			}
		}
		public static User UserData
		{
			get
			{
				return NavigationContext._loginData;
			}
		}
		public static ViewModelBase Model
		{
			get
			{
				return NavigationContext._model;
			}
		}
		public static ILog Logger
		{
			get
			{
				return LogManager.GetLogger("Logger");
			}
		}
		public static void SetMainWindow(MainWindow mainWindow)
		{
			NavigationContext._mainWindow = mainWindow;
		}
		public static void NavigateToViewModel(LoginViewModel model)
		{
			NavigationContext._model = model;
			LoginPage page = new LoginPage(model);
			NavigationContext.NavigateToViewModelBase(page);
		}
		public static void NavigateToViewModel(PlotManagementViewModel model)
		{
			NavigationContext._model = model;
			NavigationContext._plotsData = model;
			if (!(NavigationContext._mainWindow.MainFrame.Content is PlotManagementPage))
			{
				PlotManagementPage page = new PlotManagementPage(model);
				NavigationContext.NavigateToViewModelBase(page);
			}
		}
		public static void NavigateToPlotManagement()
		{
			if (NavigationContext._plotsData == null)
			{
				NavigationContext._plotsData = new PlotManagementViewModel();
			}
			NavigationContext._model = NavigationContext._plotsData;
			if (NavigationContext._plotsData.Plots != null && NavigationContext._plotsData.Plots.Count > 0)
			{
				NavigationContext._plotsData.Plots.First<PlotDetailsViewModel>().NotifyState();
			}
			NavigationContext.NavigateToViewModel(NavigationContext._plotsData);
		}
		public static void NavigateToViewModel(PlotDetailsViewModel model)
		{
			model.Refresh();
			if (NavigationContext._detailsPage == null || !NavigationContext._detailsPage.IsVisible)
			{
				NavigationContext._detailsPage = new PlotDetailsPage(new CommentsViewModel(true));
			}
			NavigationContext._model = model;
			NavigationContext._detailsPage.Model = model;
			NavigationContext._detailsPage.Show();
		}
		public static bool NavigateToChangePassword()
		{
			ChangePasswordViewModel model = new ChangePasswordViewModel();
			ChangePasswordWindows changePasswordWindows = new ChangePasswordWindows(model);
			bool? flag = changePasswordWindows.ShowDialog();
			return flag.HasValue && flag.Value;
		}
		public static void NavigateToAdminPage()
		{
			NavigationContext._model = null;
			AdministrationPage page = new AdministrationPage();
			NavigationContext.NavigateToViewModelBase(page);
		}
		public static void NavigateToUsersManagement()
		{
			UsersManagementViewModel users = UsersManagementViewModel.GetUsers();
			NavigationContext._model = users;
			UsersManagementWindow usersManagementWindow = new UsersManagementWindow(users);
			usersManagementWindow.ShowDialog();
		}
		public static void NavigateToSystemSettings()
		{
			SystemSettingsWindow systemSettingsWindow = new SystemSettingsWindow();
			if (App.SettingsEditMode)
			{
				systemSettingsWindow.Show();
				return;
			}
			systemSettingsWindow.ShowDialog();
		}
		public static void NavigateToReport()
		{
			ReportWindow reportWindow = new ReportWindow(NavigationContext._plotsData.Plots.ToList<PlotDetailsViewModel>());
			reportWindow.ShowDialog();
		}
		public static int NavigateToSelectSheetWindow(IEnumerable<string> sheets)
		{
			SelectSheetWindow selectSheetWindow = new SelectSheetWindow(sheets);
			bool? flag = selectSheetWindow.ShowDialog();
			if (flag.HasValue && flag.Value)
			{
				return selectSheetWindow.SelectedSheet;
			}
			return -1;
		}
		public static PlotDetailsViewModel GotoNextPlot()
		{
			int num = NavigationContext.PlotIndex + 1;
			if (num >= NavigationContext._plotsData.Plots.Count)
			{
				num = 0;
			}
			PlotDetailsViewModel plotDetailsViewModel = NavigationContext._plotsData.Plots.ElementAt(num);
			plotDetailsViewModel.Refresh();
			NavigationContext._model = plotDetailsViewModel;
			return plotDetailsViewModel;
		}
		public static PlotDetailsViewModel GotoNextNotEnteredPlot()
		{
			PlotDetailsViewModel plotDetailsViewModel;
			do
			{
				plotDetailsViewModel = NavigationContext.GotoNextPlot();
			}
			while (plotDetailsViewModel != null && plotDetailsViewModel.Weight > 0m);
			return plotDetailsViewModel;
		}
		public static PlotDetailsViewModel GotoPreviousPlot()
		{
			int num = NavigationContext.PlotIndex - 1;
			if (num < 0)
			{
				num = NavigationContext._plotsData.Plots.Count - 1;
			}
			PlotDetailsViewModel plotDetailsViewModel = NavigationContext._plotsData.Plots.ElementAt(num);
			plotDetailsViewModel.Refresh();
			NavigationContext._model = plotDetailsViewModel;
			return plotDetailsViewModel;
		}
		private static void NavigateToViewModelBase(Page page)
		{
			NavigationContext._mainWindow.MainFrame.Navigate(page);
			if (!string.IsNullOrEmpty(page.Title))
			{
				NavigationContext._mainWindow.Title = page.Title;
			}
		}
		public static void NavigateToCommentsManagement()
		{
			CommentsManagement commentsManagement = new CommentsManagement(new CommentsViewModel(false));
			commentsManagement.ShowDialog();
		}
		public static void SetLoginUser(User user)
		{
			NavigationContext._loginData = user;
		}
		public static void LogOut()
		{
			NavigationContext._loginData = null;
		}
		public static DataAccessDataContext GetDataContext()
		{
			DataAccessDataContext dataAccessDataContext = new DataAccessDataContext(Confiruration.ConnectionString);
			dataAccessDataContext.InitialiseDatabase();
			return dataAccessDataContext;
		}
	}
}
