using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Text.RegularExpressions;
namespace Harvest2.Helpers
{
	public class SerialPortReader : IDisposable
	{
		private static readonly int[] baudRates;
		private static readonly int[] dataBits;
		private static List<KeyValuePair<Parity, string>> _availableParity;
		private static List<KeyValuePair<StopBits, string>> _availableStopBits;
		private static readonly Regex ExtractWeightRegex;
		private SerialPort _serialPort;
		public static int[] BaudRates
		{
			get
			{
				return SerialPortReader.baudRates;
			}
		}
		public static int[] DataBits
		{
			get
			{
				return SerialPortReader.dataBits;
			}
		}
		public static List<KeyValuePair<Parity, string>> AvailableParity
		{
			get
			{
				return SerialPortReader._availableParity;
			}
		}
		public static List<KeyValuePair<StopBits, string>> AvailableStopBits
		{
			get
			{
				return SerialPortReader._availableStopBits;
			}
		}
		static SerialPortReader()
		{
			SerialPortReader.baudRates = new int[]
			{
				75,
				110,
				300,
				1200,
				2400,
				9600,
				19200,
				38400,
				57600,
				115200
			};
			SerialPortReader.dataBits = new int[]
			{
				5,
				6,
				7,
				8,
				9
			};
			SerialPortReader.ExtractWeightRegex = new Regex("\\D(?<weight>\\d+?\\.?\\d*?)[^\\.\\d]");
			SerialPortReader._availableParity = new List<KeyValuePair<Parity, string>>();
			SerialPortReader._availableParity.Add(new KeyValuePair<Parity, string>(Parity.None, "None"));
			SerialPortReader._availableParity.Add(new KeyValuePair<Parity, string>(Parity.Even, "Even"));
			SerialPortReader._availableParity.Add(new KeyValuePair<Parity, string>(Parity.Mark, "Mark"));
			SerialPortReader._availableParity.Add(new KeyValuePair<Parity, string>(Parity.Odd, "Odd"));
			SerialPortReader._availableParity.Add(new KeyValuePair<Parity, string>(Parity.Space, "Space"));
			SerialPortReader._availableStopBits = new List<KeyValuePair<StopBits, string>>();
			SerialPortReader._availableStopBits.Add(new KeyValuePair<StopBits, string>(StopBits.None, "None"));
			SerialPortReader._availableStopBits.Add(new KeyValuePair<StopBits, string>(StopBits.One, "One"));
			SerialPortReader._availableStopBits.Add(new KeyValuePair<StopBits, string>(StopBits.OnePointFive, "One and a half"));
			SerialPortReader._availableStopBits.Add(new KeyValuePair<StopBits, string>(StopBits.Two, "Two"));
		}
		public static string[] GetPostNames()
		{
			return SerialPort.GetPortNames();
		}
		public SerialPortReader()
		{
			ComPortConfiguration comPortConfiguration = Confiruration.ComPortConfiguration;
			this._serialPort = new SerialPort(comPortConfiguration.PortName, comPortConfiguration.BaudRate, comPortConfiguration.Parity, comPortConfiguration.DataBits, comPortConfiguration.StopBit);
			NavigationContext.Logger.InfoFormat("Serial port reader created. Port: '{0}', baud rate: '{1}', parity: '{2}', data bits: '{3}', stop bit: '{4}';", new object[]
			{
				comPortConfiguration.PortName,
				comPortConfiguration.BaudRate,
				comPortConfiguration.Parity,
				comPortConfiguration.DataBits,
				comPortConfiguration.StopBit
			});
		}
		public decimal ReadWeight()
		{
			try
			{
				this._serialPort.Open();
				NavigationContext.Logger.Info("Sending request '<LF>P<CR>'");
				this._serialPort.Write("<LF>P<CR>");
				NavigationContext.Logger.Info("Request sent");
				this._serialPort.ReadTimeout = (int)TimeSpan.FromMinutes(2.0).TotalMilliseconds;
				NavigationContext.Logger.Info("Waiting response");
				string text = this._serialPort.ReadTo("<CR>");
				NavigationContext.Logger.InfoFormat("Response '{0}' received", text);
				decimal result = 0m;
				if (!string.IsNullOrEmpty(text))
				{
					Match match = SerialPortReader.ExtractWeightRegex.Match(text);
					if (match.Success)
					{
						result = Convert.ToDecimal(match.Groups["weight"].Value.Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator));
					}
				}
				this._serialPort.Close();
				return result;
			}
			catch (Exception message)
			{
				NavigationContext.Logger.Error(message);
			}
			return 0m;
		}
		public void Dispose()
		{
			try
			{
				if (this._serialPort != null)
				{
					if (this._serialPort.IsOpen)
					{
						this._serialPort.Close();
					}
					this._serialPort.Dispose();
				}
			}
			finally
			{
				this._serialPort = null;
			}
		}
	}
}
