using Harvest2.Exceptions;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
namespace Harvest2.Helpers
{
	public class PlotsImporter
	{
		public Func<IEnumerable<string>, int> SelectSheetAction
		{
			get;
			set;
		}
		public IEnumerable<Plot> Import(string filePath)
		{
			List<Plot> list = new List<Plot>();
			IWorkbook workbook = WorkbookFactory.Create(filePath);
			if (workbook.NumberOfSheets == 0)
			{
				return list;
			}
			ISheet sheet = null;
			if (workbook.NumberOfSheets > 1 && this.SelectSheetAction != null)
			{
				int num = this.SelectSheetAction(this.GetSheetNames(workbook));
				if (num < 0)
				{
					return null;
				}
				sheet = workbook.GetSheetAt(num);
			}
			if (sheet == null)
			{
				sheet = workbook.GetSheetAt(0);
			}
			int num2 = this.FindHeaderRow(sheet);
			if (num2 == -1)
			{
				throw new PlotImportException("Wrong import file structure");
			}
			int num3 = num2 + 1;
			IRow row;
			while ((row = sheet.GetRow(num3)) != null)
			{
				if (this.IsSeparatorRow(row))
				{
					list.Add(this.ParseSeparatorRow(row));
				}
				else
				{
					Plot plot = this.ParseRow(row);
					if (plot != null)
					{
						list.Add(plot);
					}
				}
				num3++;
			}
			return list;
		}
		private IEnumerable<string> GetSheetNames(IWorkbook workbook)
		{
			List<string> list = new List<string>();
			for (int i = 0; i < workbook.NumberOfSheets; i++)
			{
				list.Add(workbook.GetSheetName(i));
			}
			return list;
		}
		private bool IsSeparatorRow(IRow row)
		{
			ICell cell = row.Cells[2];
			return cell.CellType == CellType.STRING && cell.StringCellValue.Equals("END", StringComparison.CurrentCultureIgnoreCase);
		}
		private int FindHeaderRow(ISheet worksheet)
		{
			IEnumerator rowEnumerator = worksheet.GetRowEnumerator();
			int num = 0;
			while (rowEnumerator.MoveNext())
			{
				IRow row = rowEnumerator.Current as IRow;
				if (row.Cells != null && row.Cells.Count > 0)
				{
					ICell cell = row.Cells.First<ICell>();
					if (!string.IsNullOrEmpty(cell.StringCellValue) && cell.StringCellValue.Equals("Harv_Ord", StringComparison.CurrentCultureIgnoreCase))
					{
						return num;
					}
				}
				num++;
			}
			return -1;
		}
		private Plot ParseRow(IRow row)
		{
			Plot plot = new Plot();
			plot.HarvestOrder = this.GetIntValue(this.GetCell(row, 0));
			plot.Column = this.GetIntValue(this.GetCell(row, 1));
			plot.Row = this.GetIntValue(this.GetCell(row, 2));
			plot.PlotId = this.GetStringValue(this.GetCell(row, 3));
			if (plot.PlotId.Contains("FILL"))
			{
				return null;
			}
			plot.RootRot = (byte)this.GetIntValue(this.GetCell(row, 5));
			plot.Crop = this.GetIntValue(this.GetCell(row, 6));
			plot.Year = this.GetIntValue(this.GetCell(row, 7));
			plot.Trial = this.GetIntValue(this.GetCell(row, 8));
			plot.Location = this.GetStringValue(this.GetCell(row, 9));
			plot.PlotNumber = this.GetIntValue(this.GetCell(row, 10));
			plot.Weight = 0m;
			return plot;
		}
		private Plot ParseSeparatorRow(IRow row)
		{
			Plot plot = new Plot();
			plot.HarvestOrder = this.GetIntValue(this.GetCell(row, 0));
			string stringValue = this.GetStringValue(this.GetCell(row, 1));
			Match match = Regex.Match(stringValue, "\\d+");
			int column = 0;
			if (match.Success)
			{
				int.TryParse(match.Value, out column);
			}
			plot.Column = column;
			plot.RootRot = (byte)this.GetIntValue(this.GetCell(row, 5));
			plot.Crop = this.GetIntValue(this.GetCell(row, 6));
			plot.Year = this.GetIntValue(this.GetCell(row, 7));
			plot.Location = this.GetStringValue(this.GetCell(row, 9));
			plot.Weight = 0m;
			plot.PlotId = string.Empty;
			plot.IsSeparator = true;
			return plot;
		}
		private int GetIntValue(ICell cell)
		{
			if (cell == null || cell.CellType != CellType.NUMERIC)
			{
				return 0;
			}
			return Convert.ToInt32(cell.NumericCellValue);
		}
		private string GetStringValue(ICell cell)
		{
			if (cell == null)
			{
				return string.Empty;
			}
			switch (cell.CellType)
			{
			case CellType.NUMERIC:
				return cell.NumericCellValue.ToString();
			case CellType.STRING:
				return cell.StringCellValue;
			default:
				return string.Empty;
			}
		}
		private ICell GetCell(IRow row, int columnIndex)
		{
			return row.Cells.FirstOrDefault((ICell it) => it.ColumnIndex == columnIndex);
		}
	}
}
