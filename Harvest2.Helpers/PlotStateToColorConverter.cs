using Harvest2.Models;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
namespace Harvest2.Helpers
{
	public class PlotStateToColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			Color color = Colors.Blue;
			if (value is PlotStates)
			{
				switch ((PlotStates)value)
				{
				case PlotStates.NotEntered:
					color = Colors.Blue;
					break;
				case PlotStates.InEditing:
					color = Colors.Yellow;
					break;
				case PlotStates.Finished:
					color = Colors.Green;
					break;
				case PlotStates.FinishedWithErrors:
					color = Colors.Red;
					break;
				}
			}
			return new SolidColorBrush(color);
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
