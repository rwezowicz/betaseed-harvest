using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
namespace Harvest2.Helpers
{
	public class GridHelpers
	{
		public static readonly DependencyProperty RowCountProperty = DependencyProperty.RegisterAttached("RowCount", typeof(int), typeof(GridHelpers), new PropertyMetadata(-1, new PropertyChangedCallback(GridHelpers.RowCountChanged)));
		public static readonly DependencyProperty ColumnCountProperty = DependencyProperty.RegisterAttached("ColumnCount", typeof(int), typeof(GridHelpers), new PropertyMetadata(-1, new PropertyChangedCallback(GridHelpers.ColumnCountChanged)));
		public static readonly DependencyProperty StarRowsProperty = DependencyProperty.RegisterAttached("StarRows", typeof(string), typeof(GridHelpers), new PropertyMetadata(string.Empty, new PropertyChangedCallback(GridHelpers.StarRowsChanged)));
		public static readonly DependencyProperty StarColumnsProperty = DependencyProperty.RegisterAttached("StarColumns", typeof(string), typeof(GridHelpers), new PropertyMetadata(string.Empty, new PropertyChangedCallback(GridHelpers.StarColumnsChanged)));
		public static readonly DependencyProperty ElementRow = DependencyProperty.RegisterAttached("ElementRow", typeof(int), typeof(GridHelpers), new PropertyMetadata(0, new PropertyChangedCallback(GridHelpers.ElementRowChanged)));
		public static readonly DependencyProperty ElementCol = DependencyProperty.RegisterAttached("ElementCol", typeof(int), typeof(GridHelpers), new PropertyMetadata(0, new PropertyChangedCallback(GridHelpers.ElementColChanged)));
		public static int GetRowCount(DependencyObject obj)
		{
			return (int)obj.GetValue(GridHelpers.RowCountProperty);
		}
		public static void SetRowCount(DependencyObject obj, int value)
		{
			obj.SetValue(GridHelpers.RowCountProperty, value);
		}
		public static void RowCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if (!(obj is Grid) || (int)e.NewValue < 0)
			{
				return;
			}
			Grid grid = (Grid)obj;
			grid.RowDefinitions.Clear();
			for (int i = 0; i < (int)e.NewValue; i++)
			{
				grid.RowDefinitions.Add(new RowDefinition
				{
					Height = GridLength.Auto
				});
			}
			GridHelpers.SetStarRows(grid);
		}
		public static int GetColumnCount(DependencyObject obj)
		{
			return (int)obj.GetValue(GridHelpers.ColumnCountProperty);
		}
		public static void SetColumnCount(DependencyObject obj, int value)
		{
			obj.SetValue(GridHelpers.ColumnCountProperty, value);
		}
		public static void ColumnCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if (!(obj is Grid) || (int)e.NewValue < 0)
			{
				return;
			}
			Grid grid = (Grid)obj;
			grid.ColumnDefinitions.Clear();
			for (int i = 0; i < (int)e.NewValue; i++)
			{
				grid.ColumnDefinitions.Add(new ColumnDefinition
				{
					Width = GridLength.Auto
				});
			}
			GridHelpers.SetStarColumns(grid);
		}
		public static string GetStarRows(DependencyObject obj)
		{
			return (string)obj.GetValue(GridHelpers.StarRowsProperty);
		}
		public static void SetStarRows(DependencyObject obj, string value)
		{
			obj.SetValue(GridHelpers.StarRowsProperty, value);
		}
		public static void StarRowsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if (!(obj is Grid) || string.IsNullOrEmpty(e.NewValue.ToString()))
			{
				return;
			}
			GridHelpers.SetStarRows((Grid)obj);
		}
		public static string GetStarColumns(DependencyObject obj)
		{
			return (string)obj.GetValue(GridHelpers.StarColumnsProperty);
		}
		public static void SetStarColumns(DependencyObject obj, string value)
		{
			obj.SetValue(GridHelpers.StarColumnsProperty, value);
		}
		public static void StarColumnsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if (!(obj is Grid) || string.IsNullOrEmpty(e.NewValue.ToString()))
			{
				return;
			}
			GridHelpers.SetStarColumns((Grid)obj);
		}
		public static int GetElementRow(DependencyObject obj)
		{
			return (int)obj.GetValue(GridHelpers.ElementRow);
		}
		public static void SetElementRow(DependencyObject obj, int value)
		{
			obj.SetValue(GridHelpers.ElementRow, value);
		}
		public static void ElementRowChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if ((int)e.NewValue < 0)
			{
				return;
			}
			obj.SetValue(Grid.RowProperty, (int)e.NewValue);
		}
		public static int GetElementCol(DependencyObject obj)
		{
			return (int)obj.GetValue(GridHelpers.ElementCol);
		}
		public static void SetElementCol(DependencyObject obj, int value)
		{
			obj.SetValue(GridHelpers.ElementCol, value);
		}
		public static void ElementColChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if ((int)e.NewValue < 0)
			{
				return;
			}
			obj.SetValue(Grid.ColumnProperty, (int)e.NewValue);
		}
		private static void SetStarColumns(Grid grid)
		{
			string[] source = GridHelpers.GetStarColumns(grid).Split(new char[]
			{
				','
			});
			for (int i = 0; i < grid.ColumnDefinitions.Count; i++)
			{
				if (source.Contains(i.ToString()))
				{
					grid.ColumnDefinitions[i].Width = new GridLength(1.0, GridUnitType.Star);
				}
			}
		}
		private static void SetStarRows(Grid grid)
		{
			string[] source = GridHelpers.GetStarRows(grid).Split(new char[]
			{
				','
			});
			for (int i = 0; i < grid.RowDefinitions.Count; i++)
			{
				if (source.Contains(i.ToString()))
				{
					grid.RowDefinitions[i].Height = new GridLength(1.0, GridUnitType.Star);
				}
			}
		}
	}
}
