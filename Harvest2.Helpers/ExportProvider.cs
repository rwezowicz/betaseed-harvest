using ExcelLibrary.SpreadSheet;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Harvest2.Helpers
{
	public class ExportProvider
	{
		public void Export(string filePath, IEnumerable<Plot> plots)
		{
			Workbook workbook = new Workbook();
			Worksheet worksheet = new Worksheet("data");
			this.WriteWorksheetHeader(worksheet);
			int num = 1;
			IOrderedEnumerable<Plot> orderedEnumerable = 
				from it in plots
				orderby it.HarvestOrder
				select it;
			string arg_43_0 = string.Empty;
			foreach (Plot current in orderedEnumerable)
			{
				if (current.IsSeparator)
				{
					this.WriteSeparatorRow(worksheet, current, num++);
				}
				else
				{
					this.WritePlotRow(worksheet, current, num++);
					int arg_85_0 = current.HarvestOrder;
					string arg_8D_0 = current.Location;
				}
			}
			this.InsertDummyRows(worksheet);
			workbook.Worksheets.Add(worksheet);
			workbook.Save(filePath);
		}
		private void WriteWorksheetHeader(Worksheet worksheet)
		{
			worksheet.Cells[0, 0] = new Cell("Harv_Ord", CellFormat.General);
			worksheet.Cells[0, 1] = new Cell("Range", CellFormat.General);
			worksheet.Cells[0, 2] = new Cell("Row", CellFormat.General);
			worksheet.Cells[0, 3] = new Cell("Plot_ID", CellFormat.General);
			worksheet.Cells[0, 4] = new Cell("Weight", CellFormat.General);
			worksheet.Cells[0, 5] = new Cell("RootRot", CellFormat.General);
			worksheet.Cells[0, 6] = new Cell("Crop", CellFormat.General);
			worksheet.Cells[0, 7] = new Cell("Year", CellFormat.General);
			worksheet.Cells[0, 8] = new Cell("Trial-No.", CellFormat.General);
			worksheet.Cells[0, 9] = new Cell("Location", CellFormat.General);
			worksheet.Cells[0, 10] = new Cell("Plot-No.", CellFormat.General);
			worksheet.Cells[0, 11] = new Cell("Comment", CellFormat.General);
		}
		private void WriteSeparatorRow(Worksheet worksheet, Plot plot, int row)
		{
			worksheet.Cells[row, 0] = new Cell(plot.HarvestOrder);
			worksheet.Cells[row, 1] = new Cell(plot.Column.ToString() + "a");
			worksheet.Cells[row, 2] = new Cell("END");
			worksheet.Cells[row, 3] = new Cell("END");
			worksheet.Cells[row, 4] = new Cell(0);
			worksheet.Cells[row, 5] = new Cell(plot.RootRot);
			worksheet.Cells[row, 6] = new Cell(plot.Crop);
			worksheet.Cells[row, 7] = new Cell(plot.Year);
			worksheet.Cells[row, 8] = new Cell("END");
			worksheet.Cells[row, 9] = new Cell(plot.Location);
			worksheet.Cells[row, 10] = new Cell(string.Empty);
			worksheet.Cells[row, 11] = new Cell(string.Empty);
		}
		private void WritePlotRow(Worksheet worksheet, Plot plot, int row)
		{
			worksheet.Cells[row, 0] = new Cell(plot.HarvestOrder);
			worksheet.Cells[row, 1] = new Cell(plot.Column);
			worksheet.Cells[row, 2] = new Cell(plot.Row);
			worksheet.Cells[row, 3] = new Cell(plot.PlotId);
			worksheet.Cells[row, 4] = new Cell(plot.Weight);
			worksheet.Cells[row, 5] = new Cell(plot.RootRot);
			worksheet.Cells[row, 6] = new Cell(plot.Crop);
			worksheet.Cells[row, 7] = new Cell(plot.Year);
			worksheet.Cells[row, 8] = new Cell(plot.Trial);
			worksheet.Cells[row, 9] = new Cell(plot.Location);
			worksheet.Cells[row, 10] = new Cell(plot.PlotNumber);
			worksheet.Cells[row, 11] = new Cell(plot.Comment);
		}
		private void InsertDummyRows(Worksheet worksheet)
		{
			int count = worksheet.Cells.Rows.Count;
			for (int i = count; i < 200; i++)
			{
				worksheet.Cells[i, 0] = new Cell(string.Empty);
			}
		}
		private void AddCell(Worksheet worksheet, int row, int coll, object value)
		{
			worksheet.Cells.CreateCell(row, coll, value, 0);
		}
	}
}
