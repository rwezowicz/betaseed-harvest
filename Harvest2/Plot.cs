using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
namespace Harvest2
{
	[Table(Name = "")]
	public class Plot : INotifyPropertyChanging, INotifyPropertyChanged
	{
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(string.Empty);
		private int _Id;
		private int _HarvestOrder;
		private int _Row;
		private int _Column;
		private string _PlotId;
		private int _Trial;
		private decimal _Weight;
		private byte _RootRot;
		private int _PlotGroupId;
		private int _PlotNumber;
		private string _Comment;
		private string _Location;
		private int _Crop;
		private int _Year;
		private bool _IsSeparator;
		private bool _SavedWithErrors;
		private EntityRef<PlotGroup> _PlotGroup;
		public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;
		[Column(Storage = "_Id", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if (this._Id != value)
				{
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
				}
			}
		}
		[Column(Storage = "_HarvestOrder")]
		public int HarvestOrder
		{
			get
			{
				return this._HarvestOrder;
			}
			set
			{
				if (this._HarvestOrder != value)
				{
					this.SendPropertyChanging();
					this._HarvestOrder = value;
					this.SendPropertyChanged("HarvestOrder");
				}
			}
		}
		[Column(Storage = "_Row")]
		public int Row
		{
			get
			{
				return this._Row;
			}
			set
			{
				if (this._Row != value)
				{
					this.SendPropertyChanging();
					this._Row = value;
					this.SendPropertyChanged("Row");
				}
			}
		}
		[Column(Storage = "_Column")]
		public int Column
		{
			get
			{
				return this._Column;
			}
			set
			{
				if (this._Column != value)
				{
					this.SendPropertyChanging();
					this._Column = value;
					this.SendPropertyChanged("Column");
				}
			}
		}
		[Column(Storage = "_PlotId", CanBeNull = false)]
		public string PlotId
		{
			get
			{
				return this._PlotId;
			}
			set
			{
				if (this._PlotId != value)
				{
					this.SendPropertyChanging();
					this._PlotId = value;
					this.SendPropertyChanged("PlotId");
				}
			}
		}
		[Column(Storage = "_Trial")]
		public int Trial
		{
			get
			{
				return this._Trial;
			}
			set
			{
				if (this._Trial != value)
				{
					this.SendPropertyChanging();
					this._Trial = value;
					this.SendPropertyChanged("Trial");
				}
			}
		}
		[Column(Storage = "_Weight")]
		public decimal Weight
		{
			get
			{
				return this._Weight;
			}
			set
			{
				if (this._Weight != value)
				{
					this.SendPropertyChanging();
					this._Weight = value;
					this.SendPropertyChanged("Weight");
				}
			}
		}
		[Column(Storage = "_RootRot")]
		public byte RootRot
		{
			get
			{
				return this._RootRot;
			}
			set
			{
				if (this._RootRot != value)
				{
					this.SendPropertyChanging();
					this._RootRot = value;
					this.SendPropertyChanged("RootRot");
				}
			}
		}
		[Column(Storage = "_PlotGroupId")]
		public int PlotGroupId
		{
			get
			{
				return this._PlotGroupId;
			}
			set
			{
				if (this._PlotGroupId != value)
				{
					if (this._PlotGroup.HasLoadedOrAssignedValue)
					{
						throw new ForeignKeyReferenceAlreadyHasValueException();
					}
					this.SendPropertyChanging();
					this._PlotGroupId = value;
					this.SendPropertyChanged("PlotGroupId");
				}
			}
		}
		[Column(Storage = "_PlotNumber")]
		public int PlotNumber
		{
			get
			{
				return this._PlotNumber;
			}
			set
			{
				if (this._PlotNumber != value)
				{
					this.SendPropertyChanging();
					this._PlotNumber = value;
					this.SendPropertyChanged("PlotNumber");
				}
			}
		}
		[Column(Storage = "_Comment")]
		public string Comment
		{
			get
			{
				return this._Comment;
			}
			set
			{
				if (this._Comment != value)
				{
					this.SendPropertyChanging();
					this._Comment = value;
					this.SendPropertyChanged("Comment");
				}
			}
		}
		[Column(Storage = "_Location", CanBeNull = false)]
		public string Location
		{
			get
			{
				return this._Location;
			}
			set
			{
				if (this._Location != value)
				{
					this.SendPropertyChanging();
					this._Location = value;
					this.SendPropertyChanged("Location");
				}
			}
		}
		[Column(Storage = "_Crop")]
		public int Crop
		{
			get
			{
				return this._Crop;
			}
			set
			{
				if (this._Crop != value)
				{
					this.SendPropertyChanging();
					this._Crop = value;
					this.SendPropertyChanged("Crop");
				}
			}
		}
		[Column(Storage = "_Year")]
		public int Year
		{
			get
			{
				return this._Year;
			}
			set
			{
				if (this._Year != value)
				{
					this.SendPropertyChanging();
					this._Year = value;
					this.SendPropertyChanged("Year");
				}
			}
		}
		[Column(Storage = "_IsSeparator")]
		public bool IsSeparator
		{
			get
			{
				return this._IsSeparator;
			}
			set
			{
				if (this._IsSeparator != value)
				{
					this.SendPropertyChanging();
					this._IsSeparator = value;
					this.SendPropertyChanged("IsSeparator");
				}
			}
		}
		[Column(Storage = "_SavedWithErrors")]
		public bool SavedWithErrors
		{
			get
			{
				return this._SavedWithErrors;
			}
			set
			{
				if (this._SavedWithErrors != value)
				{
					this.SendPropertyChanging();
					this._SavedWithErrors = value;
					this.SendPropertyChanged("SavedWithErrors");
				}
			}
		}
		[Association(Name = "PlotGroup_Plot", Storage = "_PlotGroup", ThisKey = "PlotGroupId", OtherKey = "Id", IsForeignKey = true)]
		public PlotGroup PlotGroup
		{
			get
			{
				return this._PlotGroup.Entity;
			}
			set
			{
				PlotGroup entity = this._PlotGroup.Entity;
				if (entity != value || !this._PlotGroup.HasLoadedOrAssignedValue)
				{
					this.SendPropertyChanging();
					if (entity != null)
					{
						this._PlotGroup.Entity = null;
						entity.Plots.Remove(this);
					}
					this._PlotGroup.Entity = value;
					if (value != null)
					{
						value.Plots.Add(this);
						this._PlotGroupId = value.Id;
					}
					else
					{
						this._PlotGroupId = 0;
					}
					this.SendPropertyChanged("PlotGroup");
				}
			}
		}
		public Plot()
		{
			this._PlotGroup = default(EntityRef<PlotGroup>);
		}
		protected virtual void SendPropertyChanging()
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, Plot.emptyChangingEventArgs);
			}
		}
		protected virtual void SendPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
