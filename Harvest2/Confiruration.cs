using Harvest2.Exceptions;
using Harvest2.Properties;
using System;
using System.Data.Common;
using System.IO;
using System.Reflection;
namespace Harvest2
{
	public static class Confiruration
	{
		private static bool _skipLogisticIntegration;
		private static string _logisticFolderPath;
		private static string _logisticResponsePath;
		private static TimeSpan _logisticResponseWait;
		private static string _connectionString;
		private static string _dbFilePath;
		private static ComPortConfiguration _comPortConfiguration;
		public static bool SkipLogisticIntegration
		{
			get
			{
				return Confiruration._skipLogisticIntegration;
			}
		}
		public static string LogisticFolderPath
		{
			get
			{
				return Confiruration._logisticFolderPath;
			}
		}
		public static string LogisticResponsePath
		{
			get
			{
				return Confiruration._logisticResponsePath;
			}
		}
		public static TimeSpan LogisticResponseWait
		{
			get
			{
				return Confiruration._logisticResponseWait;
			}
		}
		public static string ConnectionString
		{
			get
			{
				return Confiruration._connectionString;
			}
		}
		public static string DbFilePath
		{
			get
			{
				return Confiruration._dbFilePath;
			}
		}
		public static ComPortConfiguration ComPortConfiguration
		{
			get
			{
				return Confiruration._comPortConfiguration;
			}
		}
		static Confiruration()
		{
			Confiruration._skipLogisticIntegration = Confiruration.ReadSkipLogisticIntegration();
			Confiruration._logisticFolderPath = Confiruration.ReadLogisticFolderPath();
			Confiruration._logisticResponsePath = Confiruration.ReadLogisticResponseFolderPath();
			Confiruration._logisticResponseWait = Confiruration.ReadLogisticResponseWait();
			Confiruration._connectionString = Confiruration.ReadConnectionString();
			Confiruration._comPortConfiguration = ComPortConfiguration.ReadComPortConfiguration();
		}
		private static bool ReadSkipLogisticIntegration()
		{
			return false;
		}
		private static string ReadLogisticFolderPath()
		{
			return Settings.Default.LogisticDirectory;
		}
		private static string ReadLogisticResponseFolderPath()
		{
			return Settings.Default.LogisticResponseDirectory;
		}
		private static TimeSpan ReadLogisticResponseWait()
		{
			return TimeSpan.FromSeconds((double)Settings.Default.MaxSecondsToGetLogisticResponse);
		}
		private static string ReadConnectionString()
		{
			string connectionString = Settings.Default.ConnectionString;
			if (connectionString == null)
			{
				throw new ConfigException("Connection string was not found");
			}
			DbConnectionStringBuilder dbConnectionStringBuilder = new DbConnectionStringBuilder();
			dbConnectionStringBuilder.ConnectionString = connectionString;
			string text = (string)dbConnectionStringBuilder["Data Source"];
			if (string.IsNullOrEmpty(text))
			{
				throw new ConfigException("Connection string contains no data source");
			}
			AppDomain.CurrentDomain.SetData("DataDirectory", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
			if (text.Contains("|DataDirectory|"))
			{
				text = text.Replace("|DataDirectory|", "");
				text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), text);
			}
			string text2 = Confiruration.BuildPath(text);
			string directoryName = Path.GetDirectoryName(text2);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			dbConnectionStringBuilder["Data Source"] = text2;
			return dbConnectionStringBuilder.ToString();
		}
		private static string BuildPath(string path)
		{
			if (Path.IsPathRooted(path))
			{
				return path;
			}
			string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			return Path.Combine(directoryName, path);
		}
		public static void SetLogisticSettings(string requestPath, string responsePath)
		{
			Settings.Default.LogisticDirectory = requestPath;
			Settings.Default.LogisticResponseDirectory = responsePath;
			Settings.Default.Save();
		}
	}
}
