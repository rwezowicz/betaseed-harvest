using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
namespace Harvest2
{
	[Table(Name = "")]
	public class PlotGroup : INotifyPropertyChanging, INotifyPropertyChanged
	{
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(string.Empty);
		private int _Id;
		private int _OwnerId;
		private EntitySet<Plot> _Plots;
		private EntityRef<User> _User;
		public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;
		[Column(Storage = "_Id", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if (this._Id != value)
				{
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
				}
			}
		}
		[Column(Storage = "_OwnerId")]
		public int OwnerId
		{
			get
			{
				return this._OwnerId;
			}
			set
			{
				if (this._OwnerId != value)
				{
					if (this._User.HasLoadedOrAssignedValue)
					{
						throw new ForeignKeyReferenceAlreadyHasValueException();
					}
					this.SendPropertyChanging();
					this._OwnerId = value;
					this.SendPropertyChanged("OwnerId");
				}
			}
		}
		[Association(Name = "PlotGroup_Plot", Storage = "_Plots", ThisKey = "Id", OtherKey = "PlotGroupId")]
		public EntitySet<Plot> Plots
		{
			get
			{
				return this._Plots;
			}
			set
			{
				this._Plots.Assign(value);
			}
		}
		[Association(Name = "User_PlotGroup", Storage = "_User", ThisKey = "OwnerId", OtherKey = "Id", IsForeignKey = true)]
		public User User
		{
			get
			{
				return this._User.Entity;
			}
			set
			{
				User entity = this._User.Entity;
				if (entity != value || !this._User.HasLoadedOrAssignedValue)
				{
					this.SendPropertyChanging();
					if (entity != null)
					{
						this._User.Entity = null;
						entity.PlotGroups.Remove(this);
					}
					this._User.Entity = value;
					if (value != null)
					{
						value.PlotGroups.Add(this);
						this._OwnerId = value.Id;
					}
					else
					{
						this._OwnerId = 0;
					}
					this.SendPropertyChanged("User");
				}
			}
		}
		public PlotGroup()
		{
			this._Plots = new EntitySet<Plot>(new Action<Plot>(this.attach_Plots), new Action<Plot>(this.detach_Plots));
			this._User = default(EntityRef<User>);
		}
		protected virtual void SendPropertyChanging()
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, PlotGroup.emptyChangingEventArgs);
			}
		}
		protected virtual void SendPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		private void attach_Plots(Plot entity)
		{
			this.SendPropertyChanging();
			entity.PlotGroup = this;
		}
		private void detach_Plots(Plot entity)
		{
			this.SendPropertyChanging();
			entity.PlotGroup = null;
		}
	}
}
