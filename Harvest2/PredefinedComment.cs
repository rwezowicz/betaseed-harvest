using System;
using System.ComponentModel;
using System.Data.Linq.Mapping;
namespace Harvest2
{
	[Table(Name = "")]
	public class PredefinedComment : INotifyPropertyChanging, INotifyPropertyChanged
	{
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(string.Empty);
		private int _Id;
		private string _Comment;
		public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;
		[Column(Storage = "_Id", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if (this._Id != value)
				{
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
				}
			}
		}
		[Column(Storage = "_Comment", CanBeNull = false)]
		public string Comment
		{
			get
			{
				return this._Comment;
			}
			set
			{
				if (this._Comment != value)
				{
					this.SendPropertyChanging();
					this._Comment = value;
					this.SendPropertyChanged("Comment");
				}
			}
		}
		protected virtual void SendPropertyChanging()
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, PredefinedComment.emptyChangingEventArgs);
			}
		}
		protected virtual void SendPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
