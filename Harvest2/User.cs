using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
namespace Harvest2
{
	[Table(Name = "")]
	public class User : INotifyPropertyChanging, INotifyPropertyChanged
	{
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(string.Empty);
		private int _Id;
		private string _Login;
		private string _Password;
		private bool _HasAdminRights;
		private EntitySet<PlotGroup> _PlotGroups;
		public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;
		[Column(Storage = "_Id", IsPrimaryKey = true, IsDbGenerated = true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if (this._Id != value)
				{
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
				}
			}
		}
		[Column(Storage = "_Login", DbType = "nvarchar(50)", CanBeNull = false)]
		public string Login
		{
			get
			{
				return this._Login;
			}
			set
			{
				if (this._Login != value)
				{
					this.SendPropertyChanging();
					this._Login = value;
					this.SendPropertyChanged("Login");
				}
			}
		}
		[Column(Storage = "_Password", DbType = "nvarchar(50)", CanBeNull = false)]
		public string Password
		{
			get
			{
				return this._Password;
			}
			set
			{
				if (this._Password != value)
				{
					this.SendPropertyChanging();
					this._Password = value;
					this.SendPropertyChanged("Password");
				}
			}
		}
		[Column(Storage = "_HasAdminRights")]
		public bool HasAdminRights
		{
			get
			{
				return this._HasAdminRights;
			}
			set
			{
				if (this._HasAdminRights != value)
				{
					this.SendPropertyChanging();
					this._HasAdminRights = value;
					this.SendPropertyChanged("HasAdminRights");
				}
			}
		}
		[Association(Name = "User_PlotGroup", Storage = "_PlotGroups", ThisKey = "Id", OtherKey = "OwnerId")]
		public EntitySet<PlotGroup> PlotGroups
		{
			get
			{
				return this._PlotGroups;
			}
			set
			{
				this._PlotGroups.Assign(value);
			}
		}
		public User()
		{
			this._PlotGroups = new EntitySet<PlotGroup>(new Action<PlotGroup>(this.attach_PlotGroups), new Action<PlotGroup>(this.detach_PlotGroups));
		}
		protected virtual void SendPropertyChanging()
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, User.emptyChangingEventArgs);
			}
		}
		protected virtual void SendPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		private void attach_PlotGroups(PlotGroup entity)
		{
			this.SendPropertyChanging();
			entity.User = this;
		}
		private void detach_PlotGroups(PlotGroup entity)
		{
			this.SendPropertyChanging();
			entity.User = null;
		}
	}
}
