using Harvest2.Properties;
using System;
using System.IO.Ports;
namespace Harvest2
{
	public class ComPortConfiguration
	{
		public bool UseComPort
		{
			get;
			set;
		}
		public string PortName
		{
			get;
			set;
		}
		public int BaudRate
		{
			get;
			set;
		}
		public int DataBits
		{
			get;
			set;
		}
		public Parity Parity
		{
			get;
			set;
		}
		public StopBits StopBit
		{
			get;
			set;
		}
		public static ComPortConfiguration ReadComPortConfiguration()
		{
			return new ComPortConfiguration
			{
				UseComPort = ComPortConfiguration.ReadUseComPost(),
				PortName = Settings.Default.COM_PortName,
				BaudRate = ComPortConfiguration.ReadBaudRate(),
				DataBits = ComPortConfiguration.ReadDataBits(),
				Parity = ComPortConfiguration.ReadParity(),
				StopBit = ComPortConfiguration.ReadStopBits()
			};
		}
		private static bool ReadUseComPost()
		{
			return Settings.Default.COM_UseCom;
		}
		private static int ReadBaudRate()
		{
			return Settings.Default.COM_BaudRate;
		}
		private static int ReadDataBits()
		{
			return Settings.Default.COM_DataBits;
		}
		private static Parity ReadParity()
		{
			return (Parity)Settings.Default.COM_Parity;
		}
		private static StopBits ReadStopBits()
		{
			return (StopBits)Settings.Default.COM_StopBits;
		}
		public void WriteComSettings()
		{
			Settings.Default.COM_UseCom = this.UseComPort;
			Settings.Default.COM_BaudRate = this.BaudRate;
			Settings.Default.COM_DataBits = this.DataBits;
			Settings.Default.COM_Parity = (int)this.Parity;
			Settings.Default.COM_StopBits = (int)this.StopBit;
			Settings.Default.COM_PortName = this.PortName;
			Settings.Default.Save();
		}
	}
}
