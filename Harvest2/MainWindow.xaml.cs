﻿using Harvest2.Helpers;
using Harvest2.ViewModels;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
namespace Harvest2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IComponentConnector
	{
		public MainWindow()
		{
			this.InitializeComponent();
			NavigationContext.SetMainWindow(this);
		}
		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			if (App.SettingsEditMode)
			{
				base.Visibility = Visibility.Hidden;
				base.ShowInTaskbar = false;
				NavigationContext.NavigateToSystemSettings();
				return;
			}
			//NavigationContext.NavigateToViewModel(new LoginViewModel());
            NavigationContext.NavigateToPlotManagement();
		}
	}
}