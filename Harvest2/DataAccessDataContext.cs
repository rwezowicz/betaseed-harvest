using System;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
namespace Harvest2
{
	public class DataAccessDataContext : DataContext
	{
		private static MappingSource mappingSource = new AttributeMappingSource();
		public Table<User> Users
		{
			get
			{
				return base.GetTable<User>();
			}
		}
		public Table<Plot> Plots
		{
			get
			{
				return base.GetTable<Plot>();
			}
		}
		public Table<PlotGroup> PlotGroups
		{
			get
			{
				return base.GetTable<PlotGroup>();
			}
		}
		public Table<PredefinedComment> PredefinedComments
		{
			get
			{
				return base.GetTable<PredefinedComment>();
			}
		}
		public DataAccessDataContext(string connection) : base(connection, DataAccessDataContext.mappingSource)
		{
		}
		public DataAccessDataContext(IDbConnection connection) : base(connection, DataAccessDataContext.mappingSource)
		{
		}
		public DataAccessDataContext(string connection, MappingSource mappingSource) : base(connection, mappingSource)
		{
		}
		public DataAccessDataContext(IDbConnection connection, MappingSource mappingSource) : base(connection, mappingSource)
		{
		}
		public void InitialiseDatabase()
		{
			if (!base.DatabaseExists())
			{
				base.CreateDatabase();
				User user = new User();
				user.Login = "Admin";
				user.Password = "!QAZ1qaz";
				user.HasAdminRights = true;
				this.Users.InsertOnSubmit(user);
				User user2 = new User();
				user2.Login = "Test1";
				user2.Password = "123456";
				user2.HasAdminRights = false;
				this.Users.InsertOnSubmit(user2);
				User user3 = new User();
				user3.Login = "Test2";
				user3.Password = "123456";
				user3.HasAdminRights = false;
				this.Users.InsertOnSubmit(user3);
				base.SubmitChanges();
			}
		}
	}
}
