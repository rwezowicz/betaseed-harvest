﻿#pragma checksum "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8BCC2C5AF62336D8DB9B34D7B484AD52"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Harvest2.Pages {
    
    
    /// <summary>
    /// SystemSettingsWindow
    /// </summary>
    public partial class SystemSettingsWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 48 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BrowseLogisticFolder;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LogisticFolderPath;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BrowseLogisticResponseFolder;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LogisticResponseFolderPath;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox UseComBox;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox PortSelectBox;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox BaudRateSelectBox;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox DataBitsSelectBox;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ParitySelectBox;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox StopBitsBox;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveButton;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Harvest2;component/harvest2.pages/systemsettingswindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 2 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            ((Harvest2.Pages.SystemSettingsWindow)(target)).Closed += new System.EventHandler(this.Window_Closed);
            
            #line default
            #line hidden
            return;
            case 2:
            this.BrowseLogisticFolder = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.BrowseLogisticFolder.Click += new System.Windows.RoutedEventHandler(this.BrowseLogisticFolder_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.LogisticFolderPath = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.BrowseLogisticResponseFolder = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.BrowseLogisticResponseFolder.Click += new System.Windows.RoutedEventHandler(this.BrowseLogisticResponseFolder_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.LogisticResponseFolderPath = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.UseComBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 55 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.UseComBox.Checked += new System.Windows.RoutedEventHandler(this.UseComBox_Checked);
            
            #line default
            #line hidden
            
            #line 55 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.UseComBox.Unchecked += new System.Windows.RoutedEventHandler(this.UseComBox_Checked);
            
            #line default
            #line hidden
            return;
            case 7:
            this.PortSelectBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.BaudRateSelectBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.DataBitsSelectBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.ParitySelectBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.StopBitsBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.SaveButton = ((System.Windows.Controls.Button)(target));
            
            #line 99 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.SaveButton.Click += new System.Windows.RoutedEventHandler(this.SaveButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.CancelButton = ((System.Windows.Controls.Button)(target));
            
            #line 100 "..\..\..\..\Harvest2.Pages\SystemSettingsWindow.xaml"
            this.CancelButton.Click += new System.Windows.RoutedEventHandler(this.CancelButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

