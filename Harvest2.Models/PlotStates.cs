using System;
namespace Harvest2.Models
{
	public enum PlotStates
	{
		NotEntered = 1,
		InEditing,
		Finished,
		FinishedWithErrors
	}
}
